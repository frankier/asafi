from __future__ import annotations
from typing import Dict, List, Set, Tuple, TYPE_CHECKING

from lextract.mweproc.consts import WILDCARD

from .models import (
    AnalysisAnchor, AnalysisAnchorBit, SegMappingType,
    LextractTokAnalysis, Seg, SegCollection
)
from .step.models import NormSegStep


if TYPE_CHECKING:
    from .paragraph_analysis import TokenAnalysis


def token_anchor(surf, start=0) -> AnalysisAnchor:
    return AnalysisAnchor(
        [
            AnalysisAnchorBit(
                start,
                start + len(surf),
                surf,
            )
        ],
        surf,
    )


def get_seg_mapping(segs: List[str], toks_idxs, tok_anals: Dict[int, TokenAnalysis]) -> Dict[int, Tuple[int, SegMappingType]]:
    from .align.segs import subset_align
    seg_mapping: Dict[int, Tuple[int, SegMappingType]] = {}
    mapped: Set[NormSegStep] = set()
    visited: Set[NormSegStep] = set()

    def visit(node: NormSegStep):
        alignment = subset_align(list(node.segs.undasheds), segs)
        if alignment is not None:
            seg_mapping[tok_idx] = (id(node), alignment)
            mapped.add(node)
            return
        visited.add(node)
        for child in node.children:
            if child in visited or len(child.all_ancestors & mapped):
                continue
            visit(child)

    for tok_idx in toks_idxs:
        anchor, (root, _child, _alignment) = tok_anals[tok_idx]
        visit(root)

    return seg_mapping


def lextract_anchor(lextract_anc, gapped_mwe, matchings, surfs: List[str], tok_anals: Dict[int, TokenAnalysis], starts) -> Tuple[AnalysisAnchor, List[LextractTokAnalysis]]:
    tok_analyses: List[LextractTokAnalysis] = []
    bits: List[AnalysisAnchorBit] = []
    for match_idx, tok_idxs in matchings.items():
        # Build up analysis anchor bits
        bit_idxs = []
        for tok_idx in tok_idxs:
            bit_idxs.append(len(bits))
            bits.append(AnalysisAnchorBit(
                start=starts[tok_idx],
                end=starts[tok_idx] + len(surfs[tok_idx]),
                form=surfs[tok_idx]
            ))
        # Build up lextract tok analysis bit
        feats = lextract_anc["subwords"][match_idx][1]
        is_wildcard = False
        if WILDCARD in feats:
            del feats[WILDCARD]
            is_wildcard = True
        segs = list(feats.keys())
        tok_analysis = LextractTokAnalysis(
            form="FIXMENOFORMTOK",
            bit_idxs=bit_idxs,
            is_wildcard=is_wildcard,
            segs=SegCollection((Seg(seg) for seg in segs)),
            seg_mapping=get_seg_mapping(segs, tok_idxs, tok_anals),
        )
        tok_analyses.append(tok_analysis)
    return (
        AnalysisAnchor(
            form=gapped_mwe or "NOGAPPEDMWE",
            bits=bits,
        ),
        tok_analyses,
    )


def retrofit_lextract():
    # This should take one token of a lextract
    # and a derivation tree of a surface token
    # the lextract
    pass
