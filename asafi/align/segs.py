from typing import List, Optional

from asafi.models import SegMapping, SegMappingType


def subset_align(haystack: List[str], needle: List[str]) -> Optional[SegMappingType]:
    """
    Try and find all the segments in needle in haystack, in order.
    """
    result = SegMapping()
    haystack_idx = 0
    for needle_idx, seg in enumerate(needle):
        while haystack[haystack_idx] != seg:
            haystack_idx += 1
            if haystack_idx >= len(haystack):
                return None
        result.surf_idx_for[needle_idx] = haystack_idx
        haystack_idx += 1
        if haystack_idx >= len(haystack):
            return None
    return result
