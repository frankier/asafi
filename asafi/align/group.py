from typing import Dict, List, Tuple, Optional, Set
from .highlight_zone import HighlightZone, shift_hz, clip_span, shift_span, outside_span
from ..step.models import NormSegStep, Lineage
from ..models import AnalysisAnchor, Seg
from ..rule.base import AlignedRule
from .force import whole_seg_allo_sql_fms as force_match_spans
from .norm import longest_common_prefix_span
from abc import ABC, abstractmethod


class AlignmentGroupBase(ABC):
    """
    Container for alignments between steps in a derivation tree and various
    other things.
    """
    # The alignments between steps, a step and its parents typically
    alignments: Dict[int, List[List[HighlightZone]]]
    # The set of alignments where part of the lineage is pre aligned
    alignments_pre_aligned: Set[Tuple[int, int]]
    # The alignments of the segments onto the anchor
    anchor_alignments: Dict[int, List[HighlightZone]]

    def __init__(self, root):
        self.alignments = {}
        self.alignments_pre_aligned = set()
        self.anchor_alignments = {}
        self.align_all(root)

    def add_alignment(self, lineage: Lineage):
        alignment = []
        all_parent_segs = lineage.parent.segs
        all_child_segs = lineage.child.segs
        for parent_idx, child_idxs in lineage.offset_map.parent2child.items():
            if parent_idx in lineage.rule_map:
                rule = lineage.rule_map[parent_idx][1]
                if isinstance(rule, AlignedRule):
                    alignment.append(rule.alignment())
                    self.alignments_pre_aligned.add((id(lineage), parent_idx))
                    continue
            child_segs = all_child_segs[child_idxs]
            seg = all_parent_segs[parent_idx]
            match = force_match_spans(seg, child_segs)
            alignment.append(match)
        self.alignments[id(lineage)] = alignment

    def align_all(self, root: NormSegStep):
        for node in root.walk():
            for lineage in node.all_parents:
                self.add_alignment(lineage)

    def add_anchor_alignment(self, step: NormSegStep, anchor: AnalysisAnchor):
        self.anchor_alignments[id(step)] = force_match_spans(Seg(anchor.form), step.segs)

    def alignment_of(self, step: NormSegStep, idx: int, lemma: str) -> Optional[HighlightZone]:
        """
        Travels towards the root of the tree from `step` looking for `lemma`,
        which may be a whole anchor and return the highlight formed by whatever
        segment is indexed by `idx` in seg at `idx` in `step`.
        """
        prev: NormSegStep = step
        cur: NormSegStep = step
        intro_step: Optional[NormSegStep] = None
        found: Optional[str] = None
        while 1:
            lemma_found = lemma in cur.segs.strs
            if found and not lemma_found:
                intro_step = prev
                break

            if lemma_found:
                found = "segs_str"
            elif cur.anchor and cur.anchor.form == lemma:
                found = "anchor"

            prev = cur
            if cur.parent is None:
                break
            cur = cur.parent.parent

        if not found:
            return None

        if intro_step is None:
            intro_step = prev

        if found == "segs_str":
            assert intro_step is not None
            return self._alignment_according_to(step, idx, intro_step, list(intro_step.segs.strs).index(lemma))
        else:
            assert intro_step is not None
            return self._anchor_alignment_according_to(step, idx, intro_step)

    """
    def lemma_match_spans(self, terminal: NormSegStep) -> List[Dict[str, HighlightZone]]:
        lemmas = terminal.lemmas
        match_spans: List[Dict[str, HighlightZone]] = []
        for norm_seg_idx in range(len(terminal.segs)):
            lemma_map: Dict[str, HighlightZone] = {}
            # XXX: It would be less wasteful to look for lemmas by going upwards
            # through `offsetMap`
            for lemma in lemmas:
                hz = self.alignment_of(terminal, norm_seg_idx, lemma)
                if hz is None:
                    continue

                lemma_map[lemma] = hz

            match_spans.append(lemma_map)

        return match_spans

    def build_form_seg_map(self, term: NormSegStep, anchor: AnalysisAnchor) -> Tuple[Dict[int, List[int]], Set[int]]:
        # Build a map from form_idx to seg_idx
        wildcards: Set[int] = set()
        form_seg_map: Dict[int, List[int]] = {}
        anal_bit_norm_seg_map: Dict[int, List[int]] = {}
        root = term.root()
        for seg_idx in range(len(term.segs)):
            anal_bit_idx = term.idx_according_to(seg_idx, root)
            anal_bit_norm_seg_map.setdefault(anal_bit_idx, []).append(seg_idx)

        if anchor.anal_bits:
            for (anal_bit_idx, anal_bit) in enumerate(anchor.anal_bits):
                for bit_idx in anal_bit.bit_idxs:
                    form_seg_map[bit_idx] = anal_bit_norm_seg_map[anal_bit_idx]
                    if anal_bit.is_wildcard:
                        wildcards.add(bit_idx)

            return (form_seg_map, wildcards)
        else:
            return (anal_bit_norm_seg_map, wildcards)

    def term_match_spans(self, forms: List[str], terminal: NormSegStep, anchor: AnalysisAnchor) -> List[List[AnchorBitHighlightZone]]:
        form_seg_map: Dict[int, List[int]] = {}
        partitioned_norm_segs: List[List[str]] = []
        all_norm_segs = terminal.segs_undashed_str
        if terminal.root().anchor == anchor:
            form_seg_map_res = self.build_form_seg_map(terminal, anchor)
            form_seg_map = form_seg_map_res[0]
            wildcards = form_seg_map_res[1]
            for idx in range(len(forms)):
                assert idx in form_seg_map, 'form_seg_map must contain form idx'
                partitioned_norm_segs.append([all_norm_segs[seg_idx] for seg_idx in form_seg_map[idx]])

        else:
            partitioned_norm_segs.append(all_norm_segs)
            wildcards = set()

        all_match: List[List[AnchorBitHighlightZone]] = []
        for form_idx, (form, form_norm_segs) in enumerate(zip(forms, partitioned_norm_segs)):
            if form_idx in wildcards:
                # Wildcard match
                match = match_wildcard(form.lower(), form_norm_segs)
                seg_idxs = form_seg_map[form_idx]
                assert len(seg_idxs) == len(match), 'match_wildcard did not produce matching of seg_idxs length'
                for (hz, seg_idx) in zip(match, seg_idxs):
                    assert seg_idx <= len(all_match), 'Wildcard tried to skip ahead in all_match'
                    if seg_idx < len(all_match):
                        all_match[seg_idx].append((form_idx, hz))
                    else:
                        all_match.append([(form_idx, hz)])

            else:
                # Normal match
                match = force_match_spans(form, form_norm_segs)
                all_match.extend(([(form_idx, hz)] for hz in match))

        return all_match
    """

    @abstractmethod
    def _anchor_alignment_according_to(self, step: NormSegStep, idx: int, other: NormSegStep) -> Optional[HighlightZone]:
        pass

    def _alignment_according_to(self, step: NormSegStep, idx: int, other: NormSegStep, other_idx: int) -> Optional[HighlightZone]:
        steps = step.seg_route_up_to(idx, other)
        if steps is None:
            raise Exception("passed a non-ancestor to alignment_according_to")

        if len(steps) == 0 and idx != other_idx or len(steps) > 0 and steps[-1][2] != other_idx:
            return None

        seg_len = len(step.segs.payload[idx].undashed)
        ancestor_align = HighlightZone((0, seg_len), (0, seg_len))

        lineage_align_steps = []
        for lineage, child_idx, par_idx, par_idx_inner in steps:
            alignment_key = id(lineage)

            if alignment_key not in self.alignments:
                raise Exception("alignment_according_to called on NormSegStep(...) without an initialised alignment")

            alignment = self.alignments[alignment_key]
            lineage_align = alignment[par_idx][par_idx_inner]
            lineage_align_steps.append(lineage_align)

        return self._lineage_alignment_according_to(ancestor_align, steps, lineage_align_steps)

    @abstractmethod
    def _lineage_alignment_according_to(
        self, align: HighlightZone, steps: List[Tuple[Lineage, int, int, int]],
        lineage_align_steps: List[HighlightZone]
    ) -> HighlightZone:
        pass


class SinglePassAlignmentGroup(AlignmentGroupBase):
    def _anchor_alignment_according_to(self, step: NormSegStep, idx: int, other: NormSegStep) -> Optional[HighlightZone]:
        """
        steps = step.seg_route_up_to(idx, other)
        if steps is None:
            raise Exception("passed a non-ancestor to anchor_alignment_according_to")

        step_id = id(other)
        if step_id not in self.anchor_alignments:
            raise Exception("_anchor_alignment_according_to end up at NormSegStep(...) without an initialised anchorAlignment")

        ancestor_align = self.anchor_alignments[step_id][idx]

        for (lineage, child_idx, par_idx, par_idx_inner) in reversed(steps):
            alignment_key = id(lineage)

            if alignment_key not in self.alignments:
                raise Exception("_anchor_alignment_according_to called on NormSegStep(...) without an initialised alignment")

            alignment = self.alignments[alignment_key]
            parent_align = alignment[par_idx][par_idx_inner]

            ancestor_align = shift_clip(parent_align, ancestor_align)

        return ancestor_align
        """
        return None

    def _lineage_alignment_according_to(
        self, align: HighlightZone, steps: List[Tuple[Lineage, int, int, int]],
        lineage_align_steps: List[HighlightZone]
    ) -> HighlightZone:
        for lineage_align in lineage_align_steps:
            # Push left boundard rightwards
            align = shift_hz(lineage_align, align.match_span[0])
            # Clip exact match
            new_match_span = clip_span(align.match_span, lineage_align.match_span)
            # Extend logic match
            new_grey_match = (align.grey_span[0], max(align.grey_span[1], lineage_align.grey_span[1]))
            # XXX: or should it be like this?
            #else:
                # Extend logic match if its the rightmost child seg of the current par seg being matched
                # Otherwise clip it
                #new_grey_match = clip(ancestor_align.grey_span, lineage_align.grey_span)
            align = HighlightZone(new_match_span, new_grey_match)

        return align


class MaxOverlapAlignmentGroup(AlignmentGroupBase):
    def _anchor_alignment_according_to(self, step: NormSegStep, idx: int, other: NormSegStep) -> Optional[HighlightZone]:
        return None

    def _lineage_alignment_according_to(
        self, align: HighlightZone, steps: List[Tuple[Lineage, int, int, int]],
        lineage_align_steps: List[HighlightZone]
    ) -> HighlightZone:
        if not steps:
            return align
        logical_span = align.match_span
        right_edge = True
        for idx, (lineage_align, (lineage, child_idx, par_idx, par_idx_inner)) in enumerate(zip(lineage_align_steps, steps)):
            # Push left boundard rightwards
            if (id(lineage), par_idx) in self.alignments_pre_aligned:
                logical_span = outside_span(lineage_align)
            else:
                logical_span = shift_span(logical_span, lineage_align.match_span[0])
                if right_edge:
                    # Always extend or truncate logic span if our seg is still on the rightmost edge of the current par seg being matched
                    logical_span_end = lineage_align.grey_span[1]
                else:
                    # Otherwise keep our current logical span
                    logical_span_end = logical_span[1]
                logical_span = (logical_span[0], logical_span_end)
            right_edge = right_edge and len(lineage.offset_map.parent2child[par_idx]) - 1 == par_idx_inner
        top_lineage = steps[-1][0]
        top_idx = steps[-1][2]
        allomorph = top_lineage.parent.segs.payload[top_idx].undashed[logical_span[0]:logical_span[1]]
        bottom_lineage = steps[0][0]
        bottom_idx = steps[0][1]
        morpheme = bottom_lineage.child.segs.payload[bottom_idx].undashed
        exact_span = shift_span(longest_common_prefix_span(allomorph, morpheme), logical_span[0])
        return HighlightZone(exact_span, logical_span)
