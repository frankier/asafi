import ahocorasick

from asafi.vendor.omorfi import stuff2omor, stuff2labels


def rev(dic):
    res = {}
    for k, v in dic.items():
        if not v:
            continue
        res[v] = k
    return res


omor2stuff = rev(stuff2omor)
labels2stuff = rev(stuff2labels)


def mk_recover_stuff(mapping):
    auto = ahocorasick.Automaton()
    for other, stuff in mapping.items():
        auto.add_word(other, stuff)
    auto.make_automaton()

    def inner(ana):
        res = [stuff for (_pos, stuff) in auto.iter(ana)]
        return res

    return inner


get_omor_stuffs = mk_recover_stuff(omor2stuff)
get_labels_stuffs = mk_recover_stuff(labels2stuff)
