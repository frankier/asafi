from typing import List, Tuple
from .highlight_zone import HighlightZone
from .norm import common_prefix_len


def rightmost_longest_match(needle: str, haystack: str) -> Tuple[int, int]:
    best_position = len(haystack)
    best_length = 0
    for haystack_cursor in range(len(haystack) - 1, -1, -1):
        match_length = common_prefix_len(needle, haystack[haystack_cursor:])
        # XXX: Can terminate early if we match all of needle since no longer match is possible
        if match_length <= best_length:
            continue

        # match_length > best_length
        best_length = match_length
        best_position = haystack_cursor

    return (best_position, best_length)


def match_wildcard(surf_lower: str, segs: List[str]) -> List[HighlightZone]:
    assert len(segs) == 2, 'Currently only 2 seg wildcards supported'
    assert segs[0] == '___', 'Segs should start with wildcard'
    (ending_position, ending_full_match_length) = rightmost_longest_match(segs[1].lower(), surf_lower)
    ending_full_match_length = ending_position + ending_full_match_length
    return [
        HighlightZone((0, 0), (0, ending_position)),
        HighlightZone((ending_position, ending_full_match_length), (ending_full_match_length, len(surf_lower))),
    ]
