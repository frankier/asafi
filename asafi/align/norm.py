from .highlight_zone import Span


NORMALIZE_FRONT_VOWELS = {
    'y': 'u',
    'ä': 'a',
    'ö': 'o',
}


def norm_char(chr: str) -> str:
    chr_lower = chr.lower()
    if chr_lower in NORMALIZE_FRONT_VOWELS:
        return NORMALIZE_FRONT_VOWELS[chr_lower]

    return chr_lower


def norm_str(s: str) -> str:
    return "".join(norm_char(c) for c in s)


def common_prefix_len(str1: str, str2: str) -> int:
    idx = 0
    while idx < len(str1) and idx < len(str2) and norm_char(str1[idx]) == norm_char(str2[idx]):
        idx += 1
    return idx


def longest_common_prefix_span(haystack: str, needle: str) -> Span:
    cur_longest_start = 0
    cur_longest_len = 0
    haystack_idx = 0
    while haystack_idx < len(haystack) - cur_longest_len:
        match_len = common_prefix_len(haystack[haystack_idx:], needle)
        if match_len > cur_longest_len:
            cur_longest_start = haystack_idx
            cur_longest_len = match_len
        haystack_idx += 1
    return (cur_longest_start, cur_longest_start + cur_longest_len)
