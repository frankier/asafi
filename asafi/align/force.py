from __future__ import annotations
import heapq
from boltons.dictutils import FrozenDict
from dataclasses import dataclass, replace
from typing import Dict, List, Tuple, Optional, Callable, Iterable, FrozenSet
from .highlight_zone import HighlightZone
from .norm import norm_str
from ..models import Seg, SegCollection, MorphType


ALLOMORPHS: Dict[Tuple[MorphType, str], Tuple[str, ...]] = {
    # Plural
    (MorphType.inflection, "t"): ("i", "j"),
    # Partitive
    (MorphType.inflection, "ta"): ("tta", "a"),
}


# Two big problems
#
# 1. If we have to skip a bunch of characters it's not clear which solution is
#    preferable
#
# 2. Can reach the same solution many ways by applying the same moves. Easiest
#    solution is to have a cache of tried states to avoid revisiting. Could a
#    random ordering reduce the number of revisting.

def wrap_seg(
    seg: str,
    seg_cursor: int,
    seg_inner_cursor: int
) -> Tuple[int, int]:
    seg_len = len(seg)
    assert seg_inner_cursor <= seg_len, 'seg_cursor out of bounds'
    if seg_inner_cursor == seg_len:
        return (seg_cursor + 1, 0)
    else:
        return (seg_cursor, seg_inner_cursor)


@dataclass(frozen=True)
class MatchJob:
    seg_cursor: int
    seg_inner_cursor: int
    surf_cursor: int
    result: List[HighlightZone]
    seg_chars_skipped: int
    chars_skipped: int
    allomorphs: FrozenDict[int, str]
    unmatchable: FrozenSet[int]

    @classmethod
    def new(cls):
        return cls(
            seg_cursor=0,
            seg_inner_cursor=0,
            surf_cursor=0,
            result=[],
            seg_chars_skipped=0,
            chars_skipped=0,
            allomorphs=FrozenDict(),
            unmatchable=frozenset(),
        )

    def match(self, segs: SegCollection) -> MatchJob:
        "Match"
        highlight_end = self.surf_cursor + 1
        new_result = self.result[:]
        if self.seg_inner_cursor == 0:
            new_result.append(HighlightZone((self.surf_cursor, highlight_end), (self.surf_cursor, highlight_end)))
        else:
            new_result[-1] = new_result[-1].extend_match()

        new_seg_cursor, new_seg_inner_cursor = wrap_seg(
            self.get_effective_seg(segs),
            self.seg_cursor,
            self.seg_inner_cursor + 1,
        )
        return replace(
            self,
            seg_cursor=new_seg_cursor,
            seg_inner_cursor=new_seg_inner_cursor,
            surf_cursor=highlight_end,
            result=new_result,
        )

    def replace_allomorph(self, replacement: str):
        seg_cursor = self.seg_cursor
        return replace(
            self,
            allomorphs=self.allomorphs.updated({seg_cursor: replacement})
        )

    def get_effective_seg(self, segs: SegCollection) -> str:
        return self.allomorphs.get(self.seg_cursor, segs[self.seg_cursor].undashed)

    def skip_surf_char(self) -> MatchJob:
        "Skip a surface character"
        new_unmatchable = self.unmatchable
        if self.seg_inner_cursor > 0:
            # We don't want to match within a segment if we have started
            # skipping on the surfaec
            new_unmatchable |= {self.seg_cursor}
        new_result = self.result[:]
        new_result[-1] = new_result[-1].extend_grey()
        return replace(
            self,
            unmatchable=new_unmatchable,
            result=new_result,
            surf_cursor=self.surf_cursor + 1,
            chars_skipped=self.chars_skipped + 1,
        )

    def maybe_new_hz(self):
        new_result = self.result[:]
        if self.seg_inner_cursor == 0:
            new_result.append(HighlightZone((self.surf_cursor, self.surf_cursor), (self.surf_cursor, self.surf_cursor)))
        return new_result

    def skip_seg_char(self, segs: SegCollection) -> MatchJob:
        "Skip a seg character"
        new_seg_cursor, new_seg_inner_cursor = wrap_seg(
            self.get_effective_seg(segs),
            self.seg_cursor,
            self.seg_inner_cursor + 1,
        )
        return replace(
            self,
            seg_cursor=new_seg_cursor,
            seg_inner_cursor=new_seg_inner_cursor,
            result=self.maybe_new_hz(),
            seg_chars_skipped=self.seg_chars_skipped + 1,
        )

    def skip_seg_rest(self, segs: SegCollection) -> MatchJob:
        "Skip the rest of the current seg"
        seg_len = len(self.get_effective_seg(segs))
        return replace(
            self,
            seg_cursor=self.seg_cursor + 1,
            seg_inner_cursor=0,
            result=self.maybe_new_hz(),
            seg_chars_skipped=self.seg_chars_skipped + seg_len - self.seg_inner_cursor,
        )

    def seg_bound(self, segs: SegCollection) -> bool:
        return self.seg_cursor < len(segs)

    def surf_bound(self, surf: Seg) -> bool:
        return self.surf_cursor < len(surf.undashed)

    def tail_match_empty(self) -> bool:
        return len(self.result) > 0 and self.result[-1].is_empty_match()

    def __lt__(self, other: MatchJob):
        # Arbitrary ordering to satisfy heapq
        return id(self) < id(other)


BEAM_SIZE = 100
MAX_ITERS = 1000


class PriorityQueue:
    def __init__(self, match_val, beam_size=BEAM_SIZE):
        self.q = []
        self.match_val = match_val
        self.beam_size = beam_size

    def add(self, job: MatchJob):
        heapq.heappush(self.q, (self.match_val(job), job))
        del self.q[self.beam_size:]

    def pop(self):
        return heapq.heappop(self.q)[1]

    def peek(self):
        return self.q[0][1]

    def is_empty(self):
        return len(self.q) == 0


MatchRule = Callable[[MatchJob, PriorityQueue, Seg, SegCollection], None]
MatchVal = Callable[[MatchJob], float]


def match_rule(job: MatchJob, pq: PriorityQueue, surf: Seg, segs: SegCollection):
    # Match
    if not job.seg_bound(segs) or not job.surf_bound(surf) or job.seg_cursor in job.unmatchable:
        return
    effective_seg = job.get_effective_seg(segs)
    if surf.undashed[job.surf_cursor] != effective_seg[job.seg_inner_cursor]:
        return
    pq.add(job.match(segs))


def replace_allomorph_rule(job: MatchJob, pq: PriorityQueue, surf: Seg, segs: SegCollection):
    if (
        not job.seg_bound(segs)
        or not job.surf_bound(surf)
        or job.seg_inner_cursor != 0
        or job.seg_cursor in job.allomorphs
    ):
        return
    seg = segs[job.seg_cursor]
    if seg.type is None:
        seg_types = list(MorphType)
    else:
        seg_types = [seg.type]
    for seg_type in seg_types:
        allomorphs = ALLOMORPHS.get((seg_type, seg.undashed), ())
        for allomorph in allomorphs:
            pq.add(job.replace_allomorph(allomorph))


def skip_surf_char_rule(job: MatchJob, pq: PriorityQueue, surf: Seg, segs: SegCollection):
    # Skip a surface character if have a non-empty match to attach it to
    if job.surf_cursor > 0 and job.surf_bound(surf) and not job.tail_match_empty():
        pq.add(job.skip_surf_char())


def skip_surf_char_sloppy_rule(job: MatchJob, pq: PriorityQueue, surf: Seg, segs: SegCollection):
    # Skip a surface character
    if job.surf_cursor > 0 and job.surf_bound(surf):
        pq.add(job.skip_surf_char())


def skip_seg_char_rule(job: MatchJob, pq: PriorityQueue, surf: Seg, segs: SegCollection):
    # Skip a seg character only if we have matched the first character
    if job.surf_cursor > 0 and job.seg_inner_cursor > 0 and job.seg_bound(segs):
        pq.add(job.skip_seg_char(segs))


def skip_seg_char_sloppy_rule(job: MatchJob, pq: PriorityQueue, surf: Seg, segs: SegCollection):
    # Skip a seg character
    if job.surf_cursor > 0 and job.seg_bound(segs):
        pq.add(job.skip_seg_char(segs))


def skip_seg_rest_rule(job: MatchJob, pq: PriorityQueue, surf: Seg, segs: SegCollection):
    # Skip a seg character only if we have matched the first character
    if job.surf_cursor > 0 and job.seg_inner_cursor > 0 and job.seg_bound(segs):
        pq.add(job.skip_seg_rest(segs))


def skip_seg_rest_sloppy_rule(job: MatchJob, pq: PriorityQueue, surf: Seg, segs: SegCollection):
    # Skip a seg character
    if job.surf_cursor > 0 and job.seg_bound(segs):
        pq.add(job.skip_seg_rest(segs))


def norm_payload(seg: Seg) -> Seg:
    return replace(seg, payload=norm_str(seg.payload))


def match_spans(match_val: MatchVal, rules: Iterable[MatchRule], surf: Seg, segs: SegCollection) -> Optional[List[HighlightZone]]:
    surf_norm = norm_payload(surf)
    segs_norm = SegCollection(*(norm_payload(seg) for seg in segs))
    pq: PriorityQueue = PriorityQueue(match_val)
    pq.add(MatchJob.new())
    iter = 0
    while not pq.is_empty() and (pq.peek().seg_bound(segs) or pq.peek().surf_bound(surf)):
        #print("pq", pq.q)
        if iter >= MAX_ITERS:
            print('WARNING: Ran out of iters while matching spans', surf, segs)
            return None

        job: MatchJob = pq.pop()
        for match_rule in rules:
            match_rule(job, pq, surf_norm, segs_norm)

        iter += 1

    if not pq.is_empty():
        return pq.peek().result
    print('WARNING: Unable to match spans', surf, segs)
    return None


def default_match(surf: Seg, segs: SegCollection) -> List[HighlightZone]:
    return [
        HighlightZone((0, 0), (0, len(surf.payload)))
        for _ in segs
    ]


def force_match_spans(match_val: MatchVal, rules: Iterable[MatchRule], surf: Seg, segs: SegCollection) -> List[HighlightZone]:
    return match_spans(match_val, rules, surf, segs) or default_match(surf, segs)


def sq_loss_match_val(match: MatchJob):
    return match.chars_skipped * match.chars_skipped + match.seg_chars_skipped * match.seg_chars_skipped


def char_seg_sql_fms(surf: Seg, segs: SegCollection) -> List[HighlightZone]:
    return force_match_spans(sq_loss_match_val, [match_rule, skip_surf_char_rule, skip_seg_char_rule], surf, segs)


def whole_seg_sql_fms(surf: Seg, segs: SegCollection) -> List[HighlightZone]:
    return force_match_spans(sq_loss_match_val, [match_rule, skip_surf_char_rule, skip_seg_rest_rule], surf, segs)


def whole_seg_allo_sql_fms(surf: Seg, segs: SegCollection) -> List[HighlightZone]:
    return force_match_spans(sq_loss_match_val, [match_rule, skip_surf_char_rule, skip_seg_rest_rule, replace_allomorph_rule], surf, segs)
