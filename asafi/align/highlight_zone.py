from __future__ import annotations
from typing import Tuple


Span = Tuple[int, int]


class HighlightZone:
    match_span: Span
    grey_span: Span

    def __init__(self, match_span: Span, grey_span: Span):
        self.match_span = match_span
        self.grey_span = grey_span

    def extend_match(self) -> HighlightZone:
        return HighlightZone(
            (self.match_span[0], self.match_span[1] + 1),
            (self.grey_span[0], self.grey_span[1] + 1)
        )

    def extend_grey(self) -> HighlightZone:
        return HighlightZone(
            self.match_span,
            (self.grey_span[0], self.grey_span[1] + 1)
        )

    def is_empty_match(self) -> bool:
        return self.match_span[0] == self.match_span[1]

    def __repr__(self):
        return "<HighlightZone {}>".format(" ".join(str(x) for x in (self.match_span + self.grey_span)))


def outside_span(hz: HighlightZone) -> Span:
    return (min(hz.match_span[0], hz.grey_span[0]), max(hz.match_span[1], hz.grey_span[1]))


AnchorBitHighlightZone = Tuple[int, HighlightZone]


def clip_span(span: Span, mask: Span) -> Span:
    lower = max(span[0], mask[0])
    upper = min(span[1], mask[1])
    assert lower <= upper
    return (lower, upper)


def shift_span(span: Span, shift: int) -> Span:
    return (span[0] + shift, span[1] + shift)


def shift_hz(hz: HighlightZone, shift: int) -> HighlightZone:
    return HighlightZone(
        shift_span(hz.match_span, shift),
        shift_span(hz.grey_span, shift),
    )


def clip_hz(hz: HighlightZone, boundary: HighlightZone) -> HighlightZone:
    return HighlightZone(
        clip_span(hz.match_span, boundary.match_span),
        clip_span(hz.grey_span, boundary.grey_span),
    )


def shift_clip(hz: HighlightZone, boundary: HighlightZone) -> HighlightZone:
    # Push left boundard rightwards
    shifted_align = shift_hz(hz, boundary.match_span[0])
    # Trim right boundary leftwards
    return clip_hz(shifted_align, boundary)
