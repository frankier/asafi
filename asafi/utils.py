import itertools
from typing import Iterable, List

from asafi.align.highlight_zone import outside_span


def flatmap(func, *iterable):
    return itertools.chain.from_iterable(map(func, *iterable))


def inv(m):
    return {v_pos: k_pos for k_pos, v_poses in m.items() for v_pos in v_poses}


def flatten_align_onto_anscestors(term, align):
    """
    Takes a terminal node and an `AlignmentGroupBase` and produces a map from
    segments onto spans of their anscestors.
    """
    flat_align = {}
    for node in [term, *term.all_ancestors]:
        for seg_idx, seg in enumerate(node.segs.strs):
            aligns = {}
            for ansc_node in [node, *node.all_ancestors]:
                for ansc_seg in ansc_node.segs.strs:
                    highlight = align.alignment_of(node, seg_idx, ansc_seg)
                    if highlight is None:
                        continue
                    aligns[ansc_seg] = [highlight.match_span, highlight.grey_span]
            flat_align[seg] = aligns
    return flat_align


def flatten_term_align_onto_descendants(term, align):
    """
    Takes a terminal node and an `AlignmentGroupBase` and produces a map from
    spans of words onto highlight spans of themselves and their descendents.
    """
    flat_align = {}
    for node in term.all_ancestors:
        aligns = []
        print("** lemma", list(node.own_lemmas))
        for seg_idx, seg in enumerate(term.segs.strs):
            for lemma in node.own_lemmas:
                highlight = align.alignment_of(term, seg_idx, lemma)
                if highlight is None:
                    continue
                aligns.append((outside_span(highlight), seg_idx))
                flat_align[lemma] = aligns
    return flat_align


def get_starts(haystack: str, surfs: Iterable[str]) -> List[int]:
    start = 0
    starts = []
    for surf in surfs:
        start = haystack.index(surf, start)
        starts.append(start)
        start = start + len(surf)
    return starts
