from itertools import repeat
from functools import reduce
from typing import FrozenSet, Tuple, Iterable, Iterator


StrConfNet = Tuple[FrozenSet[str], ...]


CMP_PATS = {
    "v": [
        ("n", "v"),
        # e.g. koe+lentää, although these should maybe all be analysed (N + N) + (N => V)
        ("r", "v"),
        # e.g. edes+auttaa, often the adverb is actually a prefix/confix/fosilised morpheme thing
    ],
    "n": [
        ("n", "n"),
        # e.g. voi+leipä
        ("a", "n"),
        # e.g. puna+viini
    ],
    "a": [
        ("a", "a"),
        # e.g. hyvän+näköinen, (almost?) always ___-n ___-inen
    ]
}


empty_conf_net: Iterator[FrozenSet[str]] = repeat(frozenset())


def conf_net_or(left: Iterable[FrozenSet[str]], right: Iterable[FrozenSet[str]]) -> StrConfNet:
    return tuple((l | r for l, r in zip(left, right)))


def conf_net_union(*conf_nets):
    return reduce(conf_net_or, conf_nets, empty_conf_net)


def gen_cmp_pats(pos: str, length: int) -> StrConfNet:
    """
    This generates a confusion matrix structure of all possible subword POS
    patterns making up a compound of particular length and POS. It might seem
    that a lattice like structure might be more appropriate, however due to the
    confusion network structure of CMP_PATS, we should always end up with a
    confusion network, which is simpler to work with.
    """
    if length == 2:
        cmp_pats = CMP_PATS[pos]
        return frozenset(cmp_pat[0] for cmp_pat in cmp_pats), frozenset(cmp_pat[1] for cmp_pat in cmp_pats)
    else:
        return conf_net_union(
            *(
                gen_cmp_pats(left, length - 1) + (frozenset((right,)),)
                for left, right in CMP_PATS[pos]
            ),
            *(
                (frozenset((left,)),) + gen_cmp_pats(right, length - 1)
                for left, right in CMP_PATS[pos]
            )
        )
