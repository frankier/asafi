from .base import RuleSource, Rule
from typing import Tuple, Iterator, Set, Optional, List, Sequence
from ..models import Seg, SegCollection
from ..align.highlight_zone import HighlightZone


SPREAD_MORPHEMES = [
    ("mmin", ["sti", "mpi"], [HighlightZone((0, 0), (0, 4)), HighlightZone((0, 0), (0, 4))]),
]


class SynthRule(Rule):
    short_name = "syn"

    def __init__(self, par_seg: str, child_segs: List[str], align: List[HighlightZone]):
        self.par_seg = par_seg
        self.child_segs = child_segs
        self.align = align

    def key(self) -> str:
        return "!SYNTH!"

    def next(self) -> Set[str]:
        return set()

    def match(self, seg: Seg) -> Tuple[bool, Optional[SegCollection]]:
        if seg.whole_tok:
            return False, None

        undashed = seg.undashed
        if undashed not in SPREAD_MORPHEMES:
            return False, None

        # XXX: We know how to align this so we should be able to mark this in
        # the result somehow
        return True, SegCollection(*(Seg(bit) for bit in SPREAD_MORPHEMES[undashed]))

    def rev_match(self, segs: Sequence[Seg]) -> Tuple[bool, Optional[Tuple[Seg, int, List[Seg]]]]:
        # TODO: more efficient lookup
        segs_undashed = [seg.undashed for seg in segs]
        for start_idx in range(len(segs) - len(self.child_segs) + 1):
            if segs_undashed[start_idx:start_idx + len(self.child_segs)] == self.child_segs:
                return True, (Seg(self.par_seg), start_idx, [Seg(s) for s in self.child_segs])
        return False, None

    def alignment(self) -> List[HighlightZone]:
        return self.align

    def __repr__(self):
        return "<SynthRule {} ~> {} >".format(self.par_seg, " ".join(self.child_segs))


synth_rules = [SynthRule(par_seg, child_segs, align) for par_seg, child_segs, align in SPREAD_MORPHEMES]


class SynthRuleSource(RuleSource[SynthRule]):
    def expand(self, form: str) -> Iterator[SynthRule]:
        return iter(synth_rules)
