from __future__ import annotations
import logging
import itertools
from dataclasses import dataclass
from typing import cast, Dict, List, Tuple, Optional, Iterator, Set, Union
from sqlalchemy.sql import text
from sqlalchemy.sql.expression import Executable
from finntk.data.wordnet_pos import POS_MAP
from ..models import MorphType, Seg, SegCollection
from ..gram.cmp_pat import conf_net_union, gen_cmp_pats, StrConfNet
from ..utils import inv
import ujson
from .base import RuleSource, Rule
import pathlib

INV_POS_MAP = inv(POS_MAP)

query_cache: Dict[str, Executable] = {}

DerivTreeRow = Tuple[str, int, List[str], Optional[int], Optional[str], Optional[str]]


def expand_deriv_tree(session, lemma_name: str) -> Iterator[DerivTreeRow]:
    dialect = session.bind.dialect.name
    if dialect not in ("sqlite", "postgresql"):
        raise ValueError(f"Unknown dialect {dialect}")
    if dialect not in query_cache:
        path = pathlib.Path(__file__).parent / f"wiki_{dialect}.sql"
        with open(path) as query_f:
            query_cache[dialect] = text(query_f.read())
    query = query_cache[dialect]
    result = session.execute(query.bindparams(lemma_name=lemma_name))
    if dialect == "sqlite":
        for row in result:
            # Postgres returns a list but SQLite returns a string
            yield cast(DerivTreeRow, (*row[:2], ujson.loads(row[2]), *row[3:]))
    else:
        return result


def get_wiktionary_derivs(session, lemma_name: str):
    results = expand_deriv_tree(session, lemma_name)
    wiktionary_derivs: Dict[str, List[WiktionaryDerivAnalysis]] = {}

    for headword_whole, headword_group in itertools.groupby(results, lambda row: row[0]):
        etys = []
        for (ety_idx, poses), ety_group in itertools.groupby(headword_group, lambda row: row[1:3]):
            subwords = []
            prev_derivation_id = None
            our_derivation_type = None
            has_derivation = False
            for subword_idx, (_, _, _, derivation_id, derivation_type, headword_part) in enumerate(ety_group):
                if prev_derivation_id is not None and derivation_id != prev_derivation_id:
                    logging.warning(f"{headword_whole}.{ety_idx} has multiple derivations {prev_derivation_id}, {derivation_id}")
                    break
                if derivation_id is None:
                    continue
                assert derivation_type is not None and headword_part is not None
                our_derivation_type = derivation_type
                has_derivation = True
                subwords.append(headword_part)
                prev_derivation_id = derivation_id
            deriv: Optional[Tuple[str, List[str]]] = None
            if has_derivation:
                assert our_derivation_type is not None
                deriv = (our_derivation_type, subwords)
            ety = (
                ety_idx,
                poses,
                deriv,
            )
            etys.append(ety)
        wiktionary_derivs.setdefault(headword_whole, []).append(WiktionaryDerivAnalysis(
            whole=headword_whole,
            etys=etys,
        ))
    return wiktionary_derivs


@dataclass
class WiktionaryDerivAnalysis:
    whole: str
    etys: List[Tuple[int, List[str], Optional[Tuple[str, List[str]]]]]


def poses_compat(wiki_poses, wn_poses):
    for wiki_pos in wiki_poses:
        if wiki_pos in INV_POS_MAP:
            if INV_POS_MAP[wiki_pos] in wn_poses:
                return True
        else:
            # Unknown POS is always allowed
            return True
    return False


def map_poses(wiki_poses, force_known=True):
    """
    Map Wiktionary POS tags to WordNet style POS tags. If `force_known` is
    true, we know it must be a known tag (one of v, n, a & r) and so never
    return None, otherwise None is returned if an unknown POS is
    encountered.
    """
    poses = []
    for wiki_pos in wiki_poses:
        if wiki_pos not in INV_POS_MAP:
            if force_known:
                # Unknown POS, but `force_known` asserted that we must be dealing
                # with one of the known POSes
                continue
            else:
                # Could be anything, return None to signify as much
                return None
        poses.append(INV_POS_MAP[wiki_pos])
    if not poses:
        if not force_known:
            raise ValueError(
                "No poses mapped when force_known=True, so empty wiki_poses has "
                "been passed --- not allowed"
            )
        # At this point we've only encountered unknown POS tags, but
        # `force_known` asserts it must be a known POS tag, so return all of them
        poses = POS_MAP.keys()
    return frozenset(poses)


class WiktionaryRule(Rule):
    short_name = "wiki"

    def __init__(self, payload: WiktionaryDerivAnalysis, ety_idx: int):
        self.payload = payload
        self.ety_idx = ety_idx

    @property
    def ety(self) -> Tuple[int, List[str], Optional[Tuple[str, List[str]]]]:
        return self.payload.etys[self.ety_idx]

    def key(self):
        return self.payload.whole

    def next(self) -> Set[str]:
        if self.ety[2] is None:
            return set()
        return set(self.ety[2][1])

    def match(self, seg: Seg) -> Tuple[bool, Optional[SegCollection]]:
        if self.payload.whole != seg.payload:
            return False, None
        ety_idx, wiki_poses, deriv = self.ety
        if seg.pos is not None and not poses_compat(wiki_poses, seg.pos):
            # Segment's POS is not compatible with this rule
            return False, None
        if deriv is None:
            return True, None

        deriv_type, child_segs = deriv
        matcher_name = "match_" + deriv_type
        matcher = getattr(self, matcher_name, self.match_other)
        return matcher(seg, child_segs, wiki_poses)

    def match_compound(self, seg, child_segs, wiki_poses):
        """
        For compounds the possible parts of speech are limited by Finnish
        grammar. We use gen_cmp_pats to get the possible subword POSes that
        make up the full compound.
        """
        wn_poses = map_poses(wiki_poses, force_known=True)
        cmp_pats: StrConfNet = conf_net_union(*(gen_cmp_pats(wn_pos, len(child_segs)) for wn_pos in wn_poses))
        segs = SegCollection(*(Seg(anal_seg, pos=pos) for anal_seg, pos in zip(child_segs, cmp_pats)))
        return True, segs

    def match_inflection(self, seg, child_segs, wiki_poses):
        """
        For inflections, the POS is usually the POS of the lemma.
        However, this might not be the case for idiom definitions. An
        example of a definition which does the desired thing is
        humalassa which has a inflection template in its Noun
        definition and idiomatic definition in its Adverb definition.
        This will result in the lemma being tagged with both Noun and
        Adverb since they are in the same etymology which may be
        acceptable for our purposes but XXX it could be more precise.

        We never deinflect a lemma, we can only decompound or remove
        derivational morphemes given a lemma.
        """
        if seg.type == MorphType.lemma:
            return False, None
        first_pos = map_poses(wiki_poses, force_known=True)
        segs = SegCollection(Seg(child_segs[0], pos=first_pos), *(Seg(anal_seg) for anal_seg in child_segs[1:]))
        return True, segs

    def match_other(self, seg, child_segs, wiki_poses):
        segs = SegCollection(*(Seg(anal_seg) for anal_seg in child_segs))
        return True, segs

    def __repr__(self):
        return f"<WiktionaryRule {self.payload.whole} => {self.ety}>"


class WiktionaryRuleSource(RuleSource[WiktionaryRule]):
    def __init__(self, session, filter_empty=True):
        self.session = session
        self.cache = {}
        self.filter_empty = filter_empty

    def expand(self, form: str) -> Iterator[WiktionaryRule]:
        if form not in self.cache:
            results = get_wiktionary_derivs(self.session, form)
            # XXX: Should check each bit not in cache?
            # XXX: Or avoid refetching what is already in cache?
            # Cache hits
            self.cache.update(results)
            if form not in results:
                # Cache misses
                self.cache[form] = []
        for wda in self.cache[form]:
            if self.filter_empty and len(wda.etys) == 1 and wda.etys[0][2] is None:
                continue
            for ety_idx in range(len(wda.etys)):
                yield WiktionaryRule(wda, ety_idx)
