from __future__ import annotations
from finntk import get_omorfi, analysis_to_subword_dicts
from finntk.data.wordnet_pos import UD_POS_MAP
from finntk.omor.anlys import dict_to_analysis, norm_word_id
from asafi.align.omor import get_omor_stuffs
from finntk.omor.anlys import normseg
from dataclasses import dataclass
from typing import Any, Dict, List, Iterator, Set, Tuple, Optional
from .base import RuleSource, Rule, EagerExpandRule, KeyedRule, Hangable, WholeMatchResult
from ..models import MorphType, Seg, SegCollection
from ..utils import inv


INV_UD_POS_MAP = inv(UD_POS_MAP)


def is_unknown(subword_dicts):
    return 'OOV' in subword_dicts[0] or subword_dicts[0].get('guess') == 'UNKNOWN'


def get_analyses(token, analyses):
    for idx, analysis in enumerate(analyses):
        subword_dicts = list(analysis_to_subword_dicts(analysis.raw))
        if is_unknown(subword_dicts):
            continue
        subwords = []
        for subword_dict in subword_dicts:
            subword_analysis = dict_to_analysis(subword_dict)
            gen_dict_analysis = subword_dict.copy()
            if 'weight' in gen_dict_analysis:
                del gen_dict_analysis['weight']
            gen_analysis = dict_to_analysis(gen_dict_analysis)
            omorfi = get_omorfi()
            subwords.append(OmorfiAnaAnalysisSubword(
                ana=subword_dict,
                raw=subword_analysis,
                stuffs=get_omor_stuffs(subword_analysis),
                surf=omorfi.generate(gen_analysis),
                lemma=norm_word_id(subword_dict['word_id']),
                normsegs=list(normseg(subword_dict)),
                upos=subword_dict['upos'],
            ))
        yield OmorfiAnaAnalysis(
            raw=analysis.raw,
            stuffs=get_omor_stuffs(analysis.raw),
            subwords=subwords,
            lemma_set=[],
        )


def get_omorfi_analyses(word_form):
    omorfi = get_omorfi()
    omorfi_analyses = omorfi.analyse(word_form)

    analyses = list(get_analyses(word_form, omorfi_analyses))
    return analyses


@dataclass
class OmorfiAnaAnalysisSubword:
    ana: Dict[str, str]
    raw: str
    stuffs: List[str]
    surf: List[str]
    lemma: str
    normsegs: List[str]
    upos: str


@dataclass
class OmorfiAnaAnalysis:
    raw: str
    stuffs: List[str]
    subwords: List[OmorfiAnaAnalysisSubword]
    lemma_set: List[str]


class OmorfiRule(Rule, EagerExpandRule, KeyedRule, Hangable):
    short_name = "omor"

    def __init__(self, payload: OmorfiAnaAnalysis):
        self.payload = payload
        self.surf = "".join((surf for subword in self.payload.subwords for surf in subword.surf))
        self.normsegs = [normseg for subword in self.payload.subwords for normseg in subword.normsegs]

    def key(self) -> str:
        # XXX: is this okay?
        return self.payload.raw

    def next(self) -> Set[str]:
        lemma_strs: Set[str] = set()
        for subword in self.payload.subwords:
            lemma_strs.add(subword.lemma)
        return lemma_strs

    def _iter_segs(self):
        for subword in self.payload.subwords:
            for idx, sw_seg in enumerate(subword.normsegs):
                kwargs: Dict[str, Any] = {}
                if idx == 0:
                    upos = subword.ana.get("upos")
                    pos = None
                    if upos is not None:
                        pos = INV_UD_POS_MAP.get(upos)
                        if pos is not None:
                            kwargs["pos"] = frozenset((pos,))
                    kwargs["type"] = MorphType.lemma
                yield Seg(sw_seg, **kwargs)

    def match(self, seg: Seg) -> Tuple[bool, Optional[SegCollection]]:
        if not seg.whole_tok or self.surf != seg.payload:
            return False, None
        return True, SegCollection(*self._iter_segs())

    def match_whole(self, segs: SegCollection) -> Tuple[WholeMatchResult, int]:
        diff = len(self.normsegs) - len(segs)
        if len(segs) > len(self.normsegs):
            return WholeMatchResult.TOOFAR, diff
        elif self.normsegs == [seg.payload for seg in segs]:
            return WholeMatchResult.FOUND, diff
        else:
            return WholeMatchResult.NOTFOUND, diff

    def match_len(self) -> int:
        return len(self.normsegs)

    def __repr__(self):
        return f"<OmorfiRule {self.payload!r}>"


class OmorfiRuleSource(RuleSource[OmorfiRule]):
    def expand(self, form: str) -> Iterator[OmorfiRule]:
        results = get_omorfi_analyses(form)
        return (OmorfiRule(oaa) for oaa in results)
