from .base import RuleSource, Rule
from typing import Tuple, Iterator, Set, Optional, List
from ..models import Seg, SegCollection


class WildcardRule(Rule):
    short_name = "wild"

    def key(self) -> str:
        return "!WILDCARD!"

    def next(self) -> Set[str]:
        return set()

    def match(self, seg: Seg) -> Tuple[bool, Optional[SegCollection]]:
        if not seg.whole_tok:
            return False, None

        if seg.payload[:4] != "___-":
            return False, None

        return True, SegCollection(Seg("___", is_wildcard=True), Seg(seg.payload[4:]))

    def __repr__(self):
        return f"<WildcardRule>"


wildcard_rule = WildcardRule()


class WildcardRuleSource(RuleSource[WildcardRule]):
    def expand(self, form: str) -> Iterator[WildcardRule]:
        return iter([wildcard_rule])
