from .base import RuleSource, Rule
from typing import Tuple, Iterator, Set, Optional, List
from ..models import Seg, SegCollection


class DashRule(Rule):
    short_name = "dash"

    def key(self) -> str:
        return "!DASH!"

    def next(self) -> Set[str]:
        return set()

    def match(self, seg: Seg) -> Tuple[bool, Optional[SegCollection]]:
        if not seg.whole_tok:
            return False, None

        undashed = seg.undashed
        if "-" not in undashed:
            return False, None

        return True, SegCollection(*(Seg(bit) for bit in seg.payload.split("-")))

    def __repr__(self):
        return f"<DashRule>"


dash_rule = DashRule()


class DashRuleSource(RuleSource[DashRule]):
    def expand(self, form: str) -> Iterator[DashRule]:
        return iter([dash_rule])
