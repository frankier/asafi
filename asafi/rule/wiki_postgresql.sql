WITH RECURSIVE cte (derived_headword, ety_idx, poses, derivation_id, derivation_type, derivation_seg_id, seg_headword)
AS (
    SELECT
      der_head.name AS derived_headword,
      etymology.etymology_index AS ety_idx,
      etymology.poses AS poses,
      derivation.id AS derivation_id,
      derivation.type AS derivation_type,
      derivation_seg.id AS derivation_seg_id,
      seg_head.name AS seg_headword,
      /*length(der_head.name) * 2 + 3 AS maxlen,*/
      row_number() OVER (PARTITION BY seg_head.name ORDER BY derivation_seg.id) AS dup_num,
      ARRAY[der_head.name] AS headword_path
    FROM
      headword AS der_head JOIN
      etymology ON etymology.headword_id = der_head.id LEFT OUTER JOIN
      derivation ON derivation.etymology_id = etymology.id LEFT OUTER JOIN
      derivation_seg ON derivation_seg.derivation_id = derivation.id LEFT OUTER JOIN
      headword AS seg_head ON derivation_seg.derived_seg_id = seg_head.id 
    WHERE der_head.name = :lemma_name

    UNION ALL

    SELECT
      der_head.name AS derived_headword,
      etymology.etymology_index AS ety_idx,
      etymology.poses AS poses,
      derivation.id AS derivation_id,
      derivation.type AS derivation_type,
      derivation_seg.id AS derivation_seg_id,
      seg_head.name AS seg_headword,
      /*cte.maxlen - 1 AS maxlen,*/
      row_number() OVER (PARTITION BY seg_head.name ORDER BY derivation_seg_id) AS dup_num,
      array_append(cte.headword_path, cte.seg_headword) AS headword_path
    FROM
      headword AS der_head JOIN
      etymology ON etymology.headword_id = der_head.id LEFT OUTER JOIN
      derivation ON derivation.etymology_id = etymology.id LEFT OUTER JOIN
      derivation_seg ON derivation_seg.derivation_id = derivation.id LEFT OUTER JOIN
      headword AS seg_head ON derivation_seg.derived_seg_id = seg_head.id 
    JOIN cte ON der_head.name = cte.seg_headword AND cte.dup_num = 1
    WHERE /*length(der_head.name) < maxlen AND*/ der_head.name <> all(cte.headword_path)

)
SELECT DISTINCT
    ON (derived_headword, ety_idx, derivation_id, derivation_seg_id, derivation_type, seg_headword)
    derived_headword, ety_idx, poses, derivation_id, derivation_type, seg_headword
FROM cte
ORDER BY derived_headword, ety_idx, derivation_id, derivation_seg_id;
