WITH RECURSIVE cte (derived_headword, ety_idx, poses, derivation_id, derivation_type, derivation_seg_id, seg_headword, headword_path)
AS (
    SELECT
      der_head.name AS derived_headword,
      etymology.etymology_index AS ety_idx,
      etymology.poses AS poses,
      derivation.id AS derivation_id,
      derivation.type AS derivation_type,
      derivation_seg.id AS derivation_seg_id,
      seg_head.name AS seg_headword,
      json_array(der_head.name) AS headword_path
    FROM
      headword AS der_head JOIN
      etymology ON etymology.headword_id = der_head.id LEFT OUTER JOIN
      derivation ON derivation.etymology_id = etymology.id LEFT OUTER JOIN
      derivation_seg ON derivation_seg.derivation_id = derivation.id LEFT OUTER JOIN
      headword AS seg_head ON derivation_seg.derived_seg_id = seg_head.id
    WHERE der_head.name = :lemma_name

    UNION ALL

    SELECT
      der_head.name AS derived_headword,
      etymology.etymology_index AS ety_idx,
      etymology.poses AS poses,
      derivation.id AS derivation_id,
      derivation.type AS derivation_type,
      derivation_seg.id AS derivation_seg_id,
      seg_head.name AS seg_headword,
      json_insert(cte.headword_path, '$[0]', cte.seg_headword) AS headword_path
    FROM
      headword AS der_head JOIN
      cte ON der_head.name = cte.seg_headword JOIN
      etymology ON etymology.headword_id = der_head.id LEFT OUTER JOIN
      derivation ON derivation.etymology_id = etymology.id LEFT OUTER JOIN
      derivation_seg ON derivation_seg.derivation_id = derivation.id LEFT OUTER JOIN
      headword AS seg_head ON derivation_seg.derived_seg_id = seg_head.id
    WHERE instr(cte.headword_path, '"' || der_head.name || '"') = 0
)
SELECT derived_headword, ety_idx, poses, derivation_id, derivation_type, seg_headword
FROM cte
GROUP BY derived_headword, ety_idx, derivation_id, derivation_seg_id, derivation_type, seg_headword
ORDER BY derived_headword, ety_idx, derivation_id, derivation_seg_id;
