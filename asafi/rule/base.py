from enum import Enum
from typing import TypeVar, List, Tuple, Optional, Iterator, Set, Sequence
from typing_extensions import Protocol, runtime_checkable
from ..models import Seg, SegCollection
from ..align.highlight_zone import HighlightZone


class WholeMatchResult(Enum):
    FOUND = 0
    TOOFAR = 1
    NOTFOUND = 2


class Hangable(Protocol):
    def match_whole(self, segs: SegCollection) -> Tuple[WholeMatchResult, int]:
        ...

    def match_len(self) -> int:
        ...


@runtime_checkable
class KeyedRule(Protocol):
    def key(self) -> str:
        ...


class EagerExpandRule(Protocol):
    def next(self) -> Set[str]:
        ...


class Rule(Protocol):
    short_name: str

    def match(self, seg: Seg) -> Tuple[bool, Optional[SegCollection]]:
        ...


class RevRule(Protocol):
    def rev_match(self, segs: SegCollection) -> Tuple[bool, Optional[Tuple[Seg, int, SegCollection]]]:
        ...


@runtime_checkable
class AlignedRule(Protocol):
    def alignment(self) -> List[HighlightZone]:
        ...


class HangableRule(Hangable, Rule, Protocol):
    pass


class RevableRule(RevRule, Protocol):
    pass


RuleTV = TypeVar("RuleTV", bound=Rule, covariant=True)


class RuleSource(Protocol[RuleTV]):
    def expand(self, form: str) -> Iterator[RuleTV]:
        ...
