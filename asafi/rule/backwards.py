from .base import Rule
from typing import Tuple, Optional, List
from ..models import Seg, SegCollection


class BackwardsRule(Rule):
    short_name = "back"

    def __init__(self, par_seg: str, child_segs: List[str]):
        self.par_seg = par_seg
        self.child_segs = child_segs

    def match(self, seg: Seg) -> Tuple[bool, Optional[SegCollection]]:
        assert False

    def __repr__(self) -> str:
        return "<BackwardsRule {} => {}>".format(self.par_seg, " ".join(self.child_segs))
