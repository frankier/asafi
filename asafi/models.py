from __future__ import annotations

from dataclasses import dataclass
from typing import List, Optional, FrozenSet, Dict, Tuple, Any, Iterator, overload, Union, Iterable
from enum import Enum
from bidict import namedbidict


SegMapping = namedbidict("SegMapping", "surf_idx", "anal_idx")
SegMappingType = Any


@dataclass
class AnalysisAnchorBit:
    # Start position in characters
    start: int
    # End position in characters
    end: int
    # Form anchored to
    form: str


@dataclass
class AnalysisAnchor:
    # Tokens which make up this anchor
    bits: List[AnalysisAnchorBit]
    # Full form
    form: str


@dataclass
class LextractTokAnalysis:
    # Form of analytic token
    form: str
    # Indexes of surface tokens
    bit_idxs: List[int]
    # Whether it is a wildcard
    is_wildcard: bool
    # Converted into segments
    segs: SegCollection
    # For each a mapping from
    #     surface_token_idx
    #     ~> (id(normseg), {surface token seg idx <-> analaytic token seg idx})
    seg_mapping: Dict[int, Tuple[int, SegMappingType]]


class MorphType(Enum):
    lemma = 0
    inflection = 1
    derivation = 2


class SegCollection:
    payload: Tuple[Seg, ...]

    def __init__(self, *payload):
        self.payload = tuple(payload)

    @property
    def strs(self) -> Iterator[str]:
        return (seg.payload for seg in self.payload)

    @property
    def undasheds(self) -> Iterator[str]:
        return (seg.undashed for seg in self.payload)

    def __iter__(self):
        return iter(self.payload)

    def __len__(self):
        return len(self.payload)

    def __eq__(self, other) -> bool:
        return self.payload == other.payload

    def __hash__(self) -> int:
        return hash(self.payload)

    def __repr__(self) -> str:
        return f"<Segs {self.payload!r}>"

    @overload
    def __getitem__(self, idxs: int) -> Seg:
        ...

    @overload
    def __getitem__(self, idxs: Iterable[int]) -> SegCollection:
        ...

    def __getitem__(self, idxs: Union[int, Iterable[int]]) -> Union[Seg, SegCollection]:
        if isinstance(idxs, int):
            return self.payload[idxs]
        else:
            return SegCollection(*(self.payload[idx] for idx in idxs))


@dataclass(frozen=True)
class Seg:
    # This is the main string, usually it is the analytical form, although if
    # no analysis steps have been performed yet. It may contain dashess.
    # XXX: sort out when this should occur or move to flag and generate dashed
    # string when needed.
    payload: str
    # This is the surface string of the payload when it represents an analytical form
    surf: Optional[str] = None
    whole_tok: bool = False
    is_wildcard: bool = False
    #tok_start: bool = False
    #tok_end: bool = False
    type: Optional[MorphType] = None
    # What else: is lemma-like?; is subword like?; is derivational morpheme
    # like?; is synthetic like turning mmin into sti + empi?; is inflectional
    # morpheme like?
    pos: Optional[FrozenSet[str]] = None

    @property
    def undashed(self):
        return self.payload.strip("-")

    def __repr__(self):
        pos_str = ""
        if self.pos:
            pos_str = " [" + ",".join(self.pos) + "]"
        flags = []
        if self.whole_tok:
            flags.append(" whole")
        if self.is_wildcard:
            flags.append(" *")
        flags_str = "".join(flags)
        type_str = ""
        if self.type is not None:
            type_str = " " + self.type.name.title()
        return f"<Seg {self.payload}{type_str}{pos_str}{flags_str}>"


@dataclass
class SegAddr:
    seg_idx: int
    seg: Seg
