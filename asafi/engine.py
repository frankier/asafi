from abc import ABC, abstractmethod
from typing import cast, List, Set, Tuple
from .anchor import token_anchor
from .models import Seg, SegCollection
from .step.models import NormSegStep
from .step.step import Stepper
from .rule.base import Rule, RuleSource, HangableRule, RevableRule
from .rule.omorfi import OmorfiRuleSource
from .rule.wiki import WiktionaryRuleSource
from .rule.dash import DashRuleSource
from .rule.synth import SynthRuleSource
from .align.group import AlignmentGroupBase, MaxOverlapAlignmentGroup


def expand_rules(rule_sources: List[RuleSource], word_form: str) -> List[Set[Rule]]:
    seen: Set[str] = set()
    working_set: Set[str] = set()

    def add_forms(new_forms):
        for form in new_forms:
            if form not in seen:
                working_set.add(form)

    working_set.add(word_form)
    working_set.add(word_form.lower())
    all_rules: List[Set[Rule]] = [set() for i in range(len(rule_sources))]
    while len(working_set):
        cur_word_form = working_set.pop()
        seen.add(cur_word_form)
        for rs_idx, rule_source in enumerate(rule_sources):
            new_rules = rule_source.expand(cur_word_form)
            for new_rule in new_rules:
                add_forms(new_rule.next())
                all_rules[rs_idx].add(new_rule)
    return all_rules


def expand_rules_many(rule_sources: List[RuleSource], word_forms: Set[str]) -> List[Set[Rule]]:
    # XXX: This might be a bit wasteful since we can probably save a bit of
    # time by partitioning each word form to an individual token
    all_rules: List[Set[Rule]] = [set() for i in range(len(rule_sources))]
    for word_form in word_forms:
        for rules_acc, new_rules in zip(all_rules, expand_rules(rule_sources, word_form)):
            rules_acc.update(new_rules)
    return all_rules


class EngineBase(ABC):
    def analyse_tok(self, word_form: str) -> Tuple[NormSegStep, List[NormSegStep], AlignmentGroupBase]:
        root = NormSegStep(None, SegCollection(Seg(word_form, whole_tok=True)), token_anchor(word_form))
        return self.run_align(root, {word_form})

    def run_align(self, root, word_forms: Set[str]) -> Tuple[NormSegStep, List[NormSegStep], AlignmentGroupBase]:
        terms = self.run(Stepper(root), word_forms)
        align = MaxOverlapAlignmentGroup(root)
        return root, terms, align

    @abstractmethod
    def run(self, stepper: Stepper, word_forms: Set[str]) -> List[NormSegStep]:
        pass


class AllAtOnceEngine(EngineBase):
    def __init__(self, *rule_sources):
        self.rule_sources = rule_sources

    def expand_rules_many(self, word_forms: Set[str]) -> List[Rule]:
        return [rule for rules in expand_rules_many(self.rule_sources, word_forms) for rule in rules]

    def run(self, stepper: Stepper, word_forms: Set[str]) -> List[NormSegStep]:
        rules = self.expand_rules_many(word_forms)
        stepper.step_fp_front(rules)
        return stepper.end()


omorfi_rs = OmorfiRuleSource()
dash_rs = DashRuleSource()
synth_rs = SynthRuleSource()


class WiktionaryOmorfiHangFirstEngine(EngineBase):
    def __init__(self, session):
        self.wiki_rs = WiktionaryRuleSource(session)

    def run(self, stepper: Stepper, word_forms: Set[str]) -> List[NormSegStep]:
        # Want to be able to express stuff like:
        #   - Do WildcardRuleSource first then discard
        #   - Then do DashRuleSource and discard
        #   - Then run WiktionaryRuleSource
        #      - Hang all OmorfiRuleSource which can be and discard them
        #   - Are there any OmorfiRuleSource remaining? if so: apply them
        #   - Run WiktionaryRuleSource again starting from these
        #   - At this point, run SynthRuleSource
        # XXX: Eventually:
        # Check if any resulting segments are known, if not, run segmenter of last resort
        #  or try and use finnpos segmentation first and then try to do compound desegmentation
        all_rules = dict(zip(
            ["dash", "wiki", "omorfi", "synth"],
            expand_rules_many([dash_rs, self.wiki_rs, omorfi_rs, synth_rs], word_forms)
        ))
        all_rules["omorfi_tok"] = {rule for word_form in word_forms for rule in omorfi_rs.expand(word_form)}
        stepper.step_fp_front(all_rules["dash"])

        def step(rules):
            stepper.step_fp_front(rules | all_rules["synth"])
        step(all_rules["wiki"])
        omorfi_rules = cast(Set[HangableRule], all_rules["omorfi_tok"])
        while omorfi_rules:
            hung_res: Tuple[List[HangableRule], List[HangableRule]] = stepper.hang(omorfi_rules)
            hung_succ, hung_fail = hung_res
            if not hung_fail:
                break
            # get the shortest from hung_fail
            shortest_hung_fail_len = float("+inf")
            shortest_hung_fail: Set[HangableRule] = set()
            omorfi_rules = set()
            for cand_rule in hung_fail:
                rule_len = cand_rule.match_len()
                if rule_len < shortest_hung_fail_len:
                    shortest_hung_fail_len = cand_rule.match_len()
                    omorfi_rules.update(shortest_hung_fail)
                    shortest_hung_fail = {cand_rule}
                elif rule_len == shortest_hung_fail_len:
                    shortest_hung_fail.add(cand_rule)
                else:
                    omorfi_rules.add(cand_rule)
            # step forward with it from the start
            # XXX: Is this going to be a problem when multiple tokens come into play?
            stepper.push_root()
            step(shortest_hung_fail)
            step(all_rules["wiki"])
            stepper.merge()
        stepper.apply_rev_rules(cast(Set[RevableRule], all_rules["synth"]))
        return stepper.end()


def mk_all_at_once_engine(session):
    wiki_rs = WiktionaryRuleSource(session)
    return AllAtOnceEngine(wiki_rs, omorfi_rs, dash_rs, synth_rs)


def mk_omorfi_hang_engine(session):
    return WiktionaryOmorfiHangFirstEngine(session)
