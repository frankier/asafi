from finntk import get_omorfi
from lextract.keyed_db.extract import extract_toks, longest_matches
from .anchor import AnalysisAnchor, lextract_anchor, token_anchor
from .engine import EngineBase
from .models import Seg, SegCollection
from .step.models import NormSegStep
from .align.group import AlignmentGroupBase
from .utils import get_starts
import conllu
from bisect import bisect_right
from typing import Dict, List, Set, Tuple


TokenAnalysis = Tuple[AnalysisAnchor, Tuple[NormSegStep, List[NormSegStep], AlignmentGroupBase]]


class MissingPrerequisiteException(Exception):
    pass


class OutOfBoundsException(Exception):
    pass


def mwes_infos(mwe_ids):
    from sqlalchemy import select
    from lextract.mweproc.db.tables import tables
    return (
        select([
            tables["ud_mwe"].c.id,
            tables["mwe_fmt"].c.gapped_mwe,
            tables["link"].c.name,
            tables["link"].c.payload,
        ]).select_from(
            tables["ud_mwe"].join(
                tables["mwe_fmt"], tables["mwe_fmt"].c.mwe_id == tables["ud_mwe"].c.id, isouter=True
            ).join(
                tables["link"], tables["link"].c.mwe_id == tables["ud_mwe"].c.id, isouter=True
            )
        )
        .where(tables["ud_mwe"].c.id.in_(mwe_ids))
    )


class ParagraphAnalysis:
    def __init__(self, engine: EngineBase, session):
        self.engine = engine
        self.session = session
        self.starts = None
        self.surfs = None
        self.tok_anals: Dict[int, TokenAnalysis] = {}
        self.dep_sents = None
        self.dep_trees = None
        self.lextract_anals = None
        self.lextract_lookup: Dict[int, Set[int]] = {}

    def omorfi_tokenise(self, haystack: str):
        omorfi = get_omorfi()
        tokenised = omorfi.tokenise(haystack)
        surfs = [tok["surf"] for tok in tokenised]
        starts: List[int] = get_starts(haystack, surfs)
        self.add_tokenise(starts, surfs)

    def add_tokenise(self, starts, surfs):
        self.starts = starts
        self.surfs = surfs

    def add_dep_parse_raw(self, deps_raw, use_as_tokens=False, haystack=None):
        self.add_dep_parse_sents(conllu.parse(deps_raw), use_as_tokens=use_as_tokens, haystack=haystack)

    def add_dep_parse_sents(self, sents, trees=None, use_as_tokens=False, haystack=None):
        self.dep_sents = sents
        if trees is None:
            trees = []
            for sent in sents:
                trees.append(sent.to_tree())
        self.dep_trees = trees
        if use_as_tokens:
            assert haystack is not None
            forms: List[str] = []
            for sent in sents:
                for token in sent:
                    forms.append(token['form'])
            starts = get_starts(haystack, forms)
            self.add_tokenise(starts, forms)

    def _analyse_tok(self, idx: int):
        if self.surfs is None:
            raise MissingPrerequisiteException("Tried to analyse a token before tokens added")
        if idx in self.tok_anals:
            return
        word_form = self.surfs[idx]
        anchor = token_anchor(word_form, start=self.starts[idx])
        root = NormSegStep(None, SegCollection(Seg(word_form, whole_tok=True)), anchor)
        self.tok_anals[idx] = (anchor, self.engine.run_align(root, {word_form}))

    def _analyse_all_toks(self):
        """
        This method analyses all tokens ready for use by get_all_analyses and
        get_analyses_at.
        """
        for idx in range(len(self.surfs)):
            self._analyse_tok(idx)

    def _analyse_all_lextracts(self):
        """
        This method analyses all lextracts ready for use by get_all_analyses and
        get_analyses_at.
        """
        if self.lextract_anals is not None:
            return
        self.lextract_anals = []
        lextract_anchors = list(extract_toks(self.session, self.surfs))
        print("lextract_anchors", lextract_anchors)
        mwe_ids = [
            lextract_anc["ud_mwe_id"]
            for _, lextract_anc
            in lextract_anchors
        ]

        mwes_results = list(self.session.execute(mwes_infos(mwe_ids)))
        key_id_to_ud_mwe_info = {row[0]: row[1:] for row in mwes_results}
        print("key_id_to_ud_mwe_info", key_id_to_ud_mwe_info)

        for all_matchings, lextract_anc in lextract_anchors:
            print("all_matchings, lextract_anc", all_matchings, lextract_anc)
            longest_matchings = longest_matches(all_matchings)
            # XXX: Just choosing at random at the moment. Should possibly be some
            # ordering to make this consistent at least.
            matchings = longest_matchings.pop()
            matched_tokens = {tok_idx: anal_idx for anal_idx, tok_idxs in matchings.items() for tok_idx in tok_idxs}
            gapped_mwe, link_name, link_payload = key_id_to_ud_mwe_info[lextract_anc["ud_mwe_id"]]

            anal_anc, toks_anals = lextract_anchor(lextract_anc, gapped_mwe, matchings, self.surfs, self.tok_anals, self.starts)

            for match_idx, tok_idxs in matchings.items():
                for tok_idx in tok_idxs:
                    self.lextract_lookup.setdefault(tok_idx, set()).add(len(self.lextract_anals))
            root = NormSegStep(None, SegCollection(*(Seg(bit.form, whole_tok=True) for bit in anal_anc.bits)), anal_anc)
            run_result = self.engine.run_align(root, {bit.form for bit in anal_anc.bits})
            self.lextract_anals.append((anal_anc, run_result, matched_tokens, (link_name, link_payload)))
        print("lextract_lookup", self.lextract_lookup)

    def analyse_all(self):
        self._analyse_all_toks()
        self._analyse_all_lextracts()

    def get_all_analyses(self):
        self.analyse_all()
        return [*self.tok_anals.values(), *self.lextract_anals]

    def get_analyses_at(self, pos: int):
        """
        This method gets the analyses at a particular position.
        """
        if self.surfs is None:
            raise MissingPrerequisiteException(
                "Tried to analyse at position before token positions found"
            )
        token_idx = bisect_right(self.starts, pos) - 1
        self._analyse_tok(token_idx)
        if token_idx in self.tok_anals:
            yield "token", self.tok_anals[token_idx]
        else:
            raise OutOfBoundsException()
        self._analyse_all_lextracts()
        for lextract_idx in self.lextract_lookup.get(token_idx, []):
            yield "lextract", self.lextract_anals[lextract_idx]
