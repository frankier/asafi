_hook = None


def install_post_init_hook(hook):
    global _hook
    _hook = hook


_connstring = None


def set_connstring(connstring):
    global _connstring
    _connstring = connstring


_tables_created = False


def ensure_tables():
    from lextract.keyed_db.tables import extend_mweproc
    global _tables_created
    if not _tables_created:
        extend_mweproc()
        _tables_created = True


def get_session():
    from wikiparse.utils.db import get_session as get_session_inner
    session = get_session_inner(_connstring)
    ensure_tables()
    if _hook is not None:
        _hook(session)
    return session
