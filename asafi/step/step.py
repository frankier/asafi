from typing import cast, TypeVar, Dict, List, Tuple, Iterator, Optional, Sequence, Iterable, Set
from ..rule.base import RevableRule, Rule, WholeMatchResult, Hangable, KeyedRule
from ..rule.backwards import BackwardsRule
from ..utils import flatmap
from .models import NormSegStep, ExpansionRule, Lineage
from ..models import SegAddr, Seg, SegCollection


ExpansionRuleConf = Tuple[SegAddr, List[Tuple[Tuple[Rule, ...], Optional[SegCollection]]]]
Expansion = Tuple[SegAddr, Optional[SegCollection]]


class BadMatchNextSegs(Exception):
    pass


def combs(possibilites: List[ExpansionRuleConf]) -> List[List[ExpansionRule]]:
    if len(possibilites) == 0:
        return [[]]

    [(seg_addr, choices), *rest_pos] = possibilites
    return flatmap(lambda rest_chosen: map(
        lambda chosen: [
            (seg_addr, *chosen),
            *rest_chosen
        ],
        choices
    ), combs(rest_pos))


def segs_conf(segs: Iterable[Seg], rules: Iterable[Rule]) -> Iterator[ExpansionRuleConf]:
    for seg_idx, seg in enumerate(segs):
        conf: Dict[Optional[SegCollection], List[Rule]] = {}
        for rule in rules:
            matches, next_segs = rule.match(seg)
            if not matches:
                continue
            if next_segs is not None:
                if len(next_segs) == 0:
                    raise BadMatchNextSegs(f"Rule returned empty next segments: {rule!r}")
                if any((len(seg.payload) == 0 for seg in next_segs)):
                    raise BadMatchNextSegs(f"Rule returned empty segment in result match: {rule!r}")
            # XXX: possibly overly simplified loop check
            if next_segs is not None and any(seg in segs for seg in next_segs):
                continue
            conf.setdefault(next_segs, []).append(rule)
        if conf:
            yield SegAddr(seg_idx, seg), [(tuple(rules), output) for output, rules in conf.items()]


HangableTV = TypeVar("HangableTV", bound=Hangable)


class Stepper:
    def __init__(self, root: NormSegStep):
        self.root = root
        self.front = [[root]]
        self.not_front: List[Set[NormSegStep]] = [set()]

    @staticmethod
    def _step_one_node(node: NormSegStep, rules: Iterable[Rule]) -> Tuple[List[Tuple[NormSegStep, List[Rule]]], bool]:
        """
        Tries all combinations of rules on node to produce children nodes.
        Returns the newly added nodes and which those nodes at least one rule
        has been applied, which are labelled as depleted. Newly added nodes are
        paired with new sets of rules with rules with a key matching unexpanded
        nodes removed so they are not expanded later.
        """
        conf = list(segs_conf(node.segs, rules))
        new_nodes = []
        depleted = True
        for expand in combs(conf):
            addr_rule_map = {}
            for addr, rule, _ in expand:
                addr_rule_map[addr.seg_idx] = rule
            (new_segs, expandable, unexpandable) = expand_segs(node, [(addr, segs) for (addr, _, segs) in expand])
            if not expandable:
                depleted = False
                continue
            unexpandable_keys: List[str] = []
            for seg_addr, _ in unexpandable:
                unexpandable_rule = addr_rule_map[seg_addr.seg_idx]
                if isinstance(rule, KeyedRule):
                    unexpandable_keys.append(unexpandable_rule.key())
            next_rules = []
            for rule in rules:
                if isinstance(rule, KeyedRule) and rule.key() in unexpandable_keys:
                    continue
                next_rules.append(rule)
            next_node = NormSegStep(Lineage(expand, node), new_segs)
            node.add_child(next_node)
            new_nodes.append((next_node, next_rules))
        return new_nodes, depleted

    @staticmethod
    def _step_fp_node(node: NormSegStep, rules: Iterable[Rule]) -> Tuple[Set[NormSegStep], List[NormSegStep]]:
        """
        Expands outward from a node up to a fixed point. Returns those nodes
        which have had a rule applied upon them and those which appear to be
        unexpandable i.e. candidates to become terminal later on.
        """
        new_nodes, depleted = Stepper._step_one_node(node, rules)
        depleted_nodes = set()
        unexpandable_nodes = []
        if depleted:
            depleted_nodes.add(node)
        else:
            unexpandable_nodes.append(node)
        for new_node, new_rules in new_nodes:
            new_expanded, new_unexpandable = Stepper._step_fp_node(new_node, new_rules)
            depleted_nodes.update(new_expanded)
            unexpandable_nodes.extend(new_unexpandable)
        return depleted_nodes, unexpandable_nodes

    def step_fp_front(self, rules: Iterable[Rule]):
        next_front = []
        for node in self.front[-1]:
            depleted, unexpandable = self._step_fp_node(node, rules)
            self.not_front[-1].update(depleted)
            next_front.extend(unexpandable)
        self.front[-1] = next_front

    @staticmethod
    def _hang_one(node: NormSegStep, hangable: Hangable) -> WholeMatchResult:
        match, diff = hangable.match_whole(node.segs)

        if match == WholeMatchResult.FOUND:
            node.hung.append(([], hangable))
            return WholeMatchResult.FOUND
        elif match == WholeMatchResult.TOOFAR:
            return WholeMatchResult.TOOFAR
        elif match == WholeMatchResult.NOTFOUND and diff == 0:
            # Shortcut
            return WholeMatchResult.NOTFOUND

        # XXX: This could do redundant checking since children have non mutually exclusive devs
        for child in node.children:
            child_result = Stepper._hang_one(child, hangable)
            if child_result == WholeMatchResult.FOUND:
                return WholeMatchResult.FOUND
            elif child_result == WholeMatchResult.TOOFAR:
                # TODO: try and apply only some of the devs from child to node
                pass

        return WholeMatchResult.NOTFOUND

    def hang(self, hangables: Iterable[HangableTV]) -> Tuple[List[HangableTV], List[HangableTV]]:
        succ = []
        fail = []
        for hangable in hangables:
            result = self._hang_one(self.root, hangable)
            if result == WholeMatchResult.FOUND:
                succ.append(hangable)
            else:
                fail.append(hangable)
        return succ, fail

    def push_root(self):
        self.front.append([self.root])
        self.not_front.append(set())

    def push_all(self):
        self.front.append(list(self.root.walk()))

    def pop(self) -> Tuple[List[NormSegStep], Set[NormSegStep]]:
        return (self.front.pop(), self.not_front.pop())

    def peek(self) -> Tuple[List[NormSegStep], Set[NormSegStep]]:
        return (self.front[-1], self.not_front[-1])

    def _sync_front(self):
        self.front[-1] = [node for node in self.front[-1] if node not in self.not_front[-1]]

    def merge(self):
        unexpandable, depleted = self.pop()
        self.not_front[-1].update(depleted)
        self.front[-1].extend(unexpandable)
        self._sync_front()

    def end(self):
        while len(self.front) > 1:
            self.merge()
        for node in self.front[0]:
            node.make_terminal()
        return self.front[0]

    @staticmethod
    def _step_rev_rules(node: NormSegStep, rule: RevableRule):
        matched, result = rule.rev_match(node.segs)
        if not matched:
            # Note we're copying the children so we don't follow the nodes we add
            for child in list(node.children):
                Stepper._step_rev_rules(child, rule)
            return
        assert result is not None
        par_seg, child_idx, child_segs = result
        # Find common ancestor
        common_ancestor = node
        seg_state = [(idx + child_idx, seg.undashed) for idx, seg in enumerate(child_segs)]
        while 1:
            next_seg_state = []
            for seg_idx, seg in seg_state:
                if common_ancestor.segs.payload[seg_idx].undashed == seg:
                    if common_ancestor.parent is None:
                        print("WARNING: Not applying rule {!r} because could not find a common ancestor (reached root)".format(rule))
                        return
                    next_seg_idx = common_ancestor.parent.offset_map.child2parent[seg_idx][0]
                    next_seg_state.append((next_seg_idx, seg))
            if not next_seg_state:
                break
            seg_state = next_seg_state
            if common_ancestor.parent is None:
                print("WARNING: Not applying rule {!r} because could not find a common ancestor (reached root)".format(rule))
                return
            common_ancestor = common_ancestor.parent.parent
        if common_ancestor is node:
            print("WARNING: Not applying rule {!r} because could not find a common ancestor (did not get past node)".format(rule))
        # Make intermediate segs
        seg_addr = SegAddr(child_idx, par_seg)
        intermediate_segs = node.segs.payload[:child_idx] + (par_seg,) + node.segs.payload[child_idx + len(child_segs):]
        # Make fake rule connecting it to common ancestor
        common_before = 0
        while common_ancestor.segs.payload[common_before].undashed == intermediate_segs[common_before].undashed:
            common_before += 1
        common_after = -1
        while common_ancestor.segs.payload[common_after].undashed == intermediate_segs[common_after].undashed:
            common_after -= 1
        common_ancestor_after_idx = len(common_ancestor.segs) + common_after + 1
        if common_ancestor_after_idx - common_before != 1:
            print((
                "WARNING: Not applying rule {!r} because it requires adding "
                "more than one BackwardsRule from common ancestor to "
                "intermediate (before: {} after: {})"
            ).format(rule, common_before, common_ancestor_after_idx))
            return
        if common_after == -1:
            common_end = None
        else:
            common_end = common_after + 1
        rule_before = common_ancestor.segs.payload[common_before]
        rule_results = intermediate_segs[common_before:common_end]
        bwd_expansion = (
            SegAddr(common_before, rule_before),
            BackwardsRule(rule_before.payload, [s.payload for s in rule_results]),
            SegCollection(*rule_results)
        )
        # Create intemediate node and add
        intermediate = NormSegStep(Lineage([bwd_expansion], common_ancestor), SegCollection(*intermediate_segs))
        common_ancestor.children.append(intermediate)
        node.add_parent(Lineage([(seg_addr, cast(Rule, rule), child_segs)], intermediate))
        intermediate.children.append(node)

    def apply_rev_rules(self, rules: Iterable[RevableRule]):
        for rule in rules:
            self._step_rev_rules(self.root, rule)


def expand_segs(node: NormSegStep, expand: List[Expansion]) -> Tuple[SegCollection, List[Expansion], List[Expansion]]:
    unexpandable: List[Expansion] = []
    expandable: Dict[int, Tuple[SegAddr, SegCollection]] = {}
    for seg_addr, segs in expand:
        if segs is None:
            unexpandable.append((seg_addr, segs))
        else:
            expandable[seg_addr.seg_idx] = (seg_addr, segs)

    new_segs: List[Seg] = []
    for seg_idx, seg in enumerate(node.segs):
        if seg_idx in expandable:
            segs = expandable[seg_idx][1]

            for new_seg in segs:
                new_segs.append(new_seg)

        else:
            new_segs.append(seg)

    return (SegCollection(*new_segs), list(expandable.values()), unexpandable)
