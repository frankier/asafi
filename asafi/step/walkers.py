from .models import NormSegStep
from typing import List


def terminals(root: NormSegStep) -> List[NormSegStep]:
    res: List[NormSegStep] = []

    for step in root.walk():
        if step.terminal:
            res.append(step)

    return res


def lemmas(root) -> List[str]:
    res: List[str] = []

    for step in root.walk():
        for own_lemma in step.own_lemmas:
            if own_lemma in res:
                continue

            res.append(own_lemma)

    return res
