from __future__ import annotations
from dataclasses import dataclass, field
from typing import Dict, List, Tuple, Optional, Set, Iterable
from ..models import AnalysisAnchor, SegAddr, Seg, SegCollection
from ..rule.base import Rule, Hangable


ExpansionRule = Tuple[SegAddr, Rule, Optional[SegCollection]]


class OffsetMap:
    child2parent: Dict[int, Tuple[int, int]]
    parent2child: Dict[int, List[int]]

    def __init__(self, lineage: Lineage):
        self.child2parent = {}
        self.parent2child = {}
        self._populate(lineage)

    def _populate(self, lineage: Lineage):
        def next_dev_idx(dev_idx: int) -> int:
            while dev_idx < len(lineage.rules) and lineage.rules[dev_idx][2] is None:
                dev_idx += 1
            return dev_idx

        dev_idx = next_dev_idx(0)
        dev_inner_idx = 0
        par_idx = 0
        idx = 0

        def add_current():
            self.child2parent[idx] = (par_idx, dev_inner_idx)
            self.parent2child.setdefault(par_idx, []).append(idx)

        while idx < len(lineage.child.segs):
            done_dev = False
            if dev_idx < len(lineage.rules):
                dev = lineage.rules[dev_idx]
                assert dev[2] is not None
                while (
                    (par_idx + dev_inner_idx) >= dev[0].seg_idx
                    and (par_idx + dev_inner_idx) < (dev[0].seg_idx + len(dev[2]))
                ):
                    done_dev = True
                    add_current()
                    dev_inner_idx += 1
                    idx += 1
            if done_dev:
                dev_idx = next_dev_idx(dev_idx + 1)
                dev_inner_idx = 0
            else:
                add_current()
                idx += 1

            par_idx += 1

    """
    def idx_of_parent_idx(self, idx: int) -> int:
        res = 0
        parent_idx = self.offset_map[idx]
        while idx >= 0 and parent_idx == self.offset_map[idx]:
            idx -= 1
            res += 1

        return res - 1
    """


@dataclass
class Lineage:
    ## Passed at construction time
    rules: List[ExpansionRule]
    parent: NormSegStep
    _child: Optional[NormSegStep] = None

    ## Generated based on other info:
    # Index map: Each one maps the indexes of segments in this step to indexes of segments in the parent segment
    offset_map: OffsetMap = field(init=False)
    _rule_map: Optional[Dict[int, ExpansionRule]] = None

    def __post_init__(self) -> None:
        if self._child is not None:
            self.offset_map = OffsetMap(self)

    def set_child(self, child: NormSegStep):
        self._child = child
        self.offset_map = OffsetMap(self)

    @property
    def child(self) -> NormSegStep:
        assert self._child is not None
        return self._child

    @property
    def rule_map(self) -> Dict[int, ExpansionRule]:
        if self._rule_map is None:
            self._rule_map = {rule[0].seg_idx: rule for rule in self.rules}
        return self._rule_map


@dataclass(eq=False)
class NormSegStep:
    ## Passed at construction time
    # Parent node
    parent: Optional[Lineage]
    # Current normsegs
    segs: SegCollection
    # Any anchors which begin this step
    anchor: Optional[AnalysisAnchor] = None
    # Alternative parents
    alt_parents: List[Lineage] = field(default_factory=list)

    ## Mutated after construction
    # Whether this is a final analysis
    terminal: bool = False
    # Children
    children: List[NormSegStep] = field(default_factory=list)
    # Any hangable rules hung off this step
    hung: List[Tuple[List[ExpansionRule], Hangable]] = field(default_factory=list)

    ## Generated based on other info:
    # This is mainly used for checking for loops. It's the set of all segs from
    # previous generations and our own.
    prev_segs: Set[str] = field(default_factory=set)

    def __post_init__(self) -> None:
        self.prev_segs = set() if self.parent is None else set(self.parent.parent.prev_segs)
        for seg in self.segs:
            self.prev_segs.add(seg.payload)
        for parent in self.all_parents:
            parent.set_child(self)

    def add_child(self, child: NormSegStep):
        self.children.append(child)

    def add_parent(self, parent: Lineage):
        assert self.parent is not None
        parent.set_child(self)
        self.alt_parents.append(parent)

    def make_terminal(self):
        self.terminal = True

    def root(self):
        if self.parent is None:
            return self

        return self.parent[0].root()

    def route_up_to(self, other: NormSegStep) -> Optional[List[Lineage]]:
        if self is other:
            return []
        if self.is_root:
            return None
        route = None
        if self.alt_parents:
            # XXX: Currently always prefer alt_parent. Should use some
            # information to make a better decision here.

            lineage = self.alt_parents[0]
            route = lineage.parent.route_up_to(other)
        if route is None and self.parent is not None:
            lineage = self.parent
            route = lineage.parent.route_up_to(other)
        if route is not None:
            return [lineage] + route
        return None

    def seg_route_up_to(self, idx: int, other: NormSegStep) -> Optional[List[Tuple[Lineage, int, int, int]]]:
        cur_idx = idx
        route = self.route_up_to(other)
        if route is None:
            return None
        result = []
        for lineage in route:
            par_idx, par_idx_inner = lineage.offset_map.child2parent[cur_idx]
            result.append((lineage, cur_idx, par_idx, par_idx_inner))
            cur_idx = par_idx
        return result

    def idx_according_to(self, idx: int, other: NormSegStep) -> int:
        steps = self.route_up_to(other)
        cur_idx = idx

        if steps is None:
            raise Exception("passed a non-ancestor to idx_according_to")

        for lineage in reversed(steps):
            cur_idx = lineage.offset_map.child2parent[cur_idx][0]

        return cur_idx

    def walk_all(self):
        yield self
        for child in self.children:
            yield from child.walk()

    def walk(self):
        seen = set()
        for node in self.walk_all():
            id_node = id(node)
            if id_node in seen:
                continue
            seen.add(id_node)
            yield node

    def draw(self, outfn="drawn.dot", do_edge_label=False, do_tooltip=True) -> None:
        import networkx as nx
        from networkx.drawing.nx_agraph import write_dot
        G = nx.DiGraph()
        node_map: Dict[int, int] = {}
        for idx, node in enumerate(self.walk()):
            if id(node) in node_map:
                continue
            node_map[id(node)] = idx
            kwargs = {}
            if node.terminal:
                kwargs["fillcolor"] = "yellow"
            else:
                kwargs["fillcolor"] = "white"
            if do_tooltip:
                kwargs["tooltip"] = "\n".join((repr(s) for s in node.segs))
            G.add_node(
                idx,
                label=" ".join(node.segs.undasheds),
                style="filled",
                **kwargs,
            )
        for idx, node in enumerate(self.walk()):

            def tooltip(lineage: Lineage):
                res = []
                for addr, rule, result in lineage.rules:
                    res.append("{}: {} → {}\t{!r}".format(addr.seg_idx, addr.seg.payload, result and " ".join((s.payload for s in result)), rule))
                return "\n".join(res)

            def edge_label(lineage: Lineage):
                res = []
                for addr, rule, result in lineage.rules:
                    res.append("{}: {} → {}".format(rule.short_name, addr.seg.payload, result and " ".join((s.payload for s in result))))
                return "\n".join(res)

            def add_edge(lineage: Lineage, **edge_attrs):
                if do_tooltip:
                    edge_attrs["tooltip"] = tooltip(lineage)
                if do_edge_label:
                    edge_attrs["label"] = edge_label(lineage)
                G.add_edge(node_map[id(lineage.parent)], idx, **edge_attrs)

            if node.parent is not None:
                add_edge(node.parent)
            for parent in node.alt_parents:
                add_edge(parent, style="dashed")
        write_dot(G, outfn)

    @property
    def lemmas(self) -> List[str]:
        res: List[str]
        if self.parent is None:
            res = []
        else:
            res = self.parent.parent.lemmas

        for own_lemma in self.own_lemmas:
            if own_lemma in res:
                continue

            res.append(own_lemma)

        return res

    @property
    def own_lemmas(self) -> List[str]:
        res: List[str] = []
        if self.anchor is not None:
            res.append(self.anchor.form)

        for seg in self.segs.strs:
            if seg in res:
                continue

            res.append(seg)

        return res

    @property
    def is_root(self) -> bool:
        return self.parent is None

    @property
    def all_parents(self) -> Iterable[Lineage]:
        if self.parent is not None:
            yield self.parent
        yield from self.alt_parents

    @property
    def all_ancestors(self) -> Set[NormSegStep]:
        res = set()
        for parent in self.all_parents:
            res.add(parent.parent)
            res.update(parent.parent.all_ancestors)
        return res
