from typing import Tuple, List
from asafi.align.highlight_zone import HighlightZone, outside_span
from more_itertools.recipes import pairwise


def assert_zero_width(span: Tuple[int, int]):
    assert span[1] - span[0] == 0


def assert_match_spans(match_spans: List[HighlightZone], *expected):
    seg_idx = 0
    is_grey = False
    side_idx = 0
    for ex in expected:
        match_span = match_spans[seg_idx]
        if is_grey:
            assert match_span.grey_span[side_idx] == ex, f"Didn't match for {seg_idx}, grey, {side_idx}"
        else:
            assert match_span.match_span[side_idx] == ex, f"Didn't match for {seg_idx}, match, {side_idx}"
        side_idx += 1
        if side_idx == 2:
            side_idx = 0
            is_grey = not is_grey
            if not is_grey:
                seg_idx += 1


def assert_span_well_formed(hzs: List[HighlightZone], surf, segs):
    # 1. Each grey is wider than the match
    for hz in hzs:
        assert hz.grey_span[0] <= hz.match_span[0]
        assert hz.grey_span[1] >= hz.match_span[1]
    # 2. A logical match cannot go earlier than its predecesor, nor a go beyond its successor
    for prev_hz, next_hz in pairwise(hzs):
        assert prev_hz.grey_span[0] <= next_hz.grey_span[0]
        assert prev_hz.grey_span[1] <= next_hz.grey_span[1]
    # 3. The number of match spans is the same as the number of segs
    assert len(hzs) == len(segs)
    # 4. The first hz's match starts from the beginning of the surf
    assert hzs[0].grey_span[0] == 0
    # 5. The last hz's grey reaches the end of the surf
    assert hzs[-1].grey_span[1] == len(surf)


def assert_logical_spans(match_spans: List[HighlightZone], *expected):
    for match_span, (expected_left, expected_right) in zip(match_spans, expected):
        actual_left, actual_right = outside_span(match_span)
        if expected_left is not None:
            assert actual_left == expected_left
        if expected_right is not None:
            assert actual_right == expected_right


def assert_match_lengths(match_spans: List[HighlightZone], *expected):
    for match_span, expected_length in zip(match_spans, expected):
        if expected_length is not None:
            assert match_span.match_span[1] - match_span.match_span[0] == expected_length


def terminal_seg_strs(terms):
    all_segs_str = []
    for term in terms:
        all_segs_str.append(list(term.segs.undasheds))
    return all_segs_str
