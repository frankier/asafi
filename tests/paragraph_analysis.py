import pytest

from asafi.engine import mk_omorfi_hang_engine as mk_engine
from asafi.db import get_session
from asafi.paragraph_analysis import ParagraphAnalysis
from asafi.models import AnalysisAnchorBit
from asafi.utils import flatten_align_onto_anscestors


@pytest.fixture(scope="module")
def asafi():
    session = get_session()
    engine = mk_engine(session)
    return ParagraphAnalysis(engine, session)


# XXX: esim. removed because it breaks OMorFi with get_start(...)
# OMorFi produces two .s
MUU_MUASSA_TEXT = """
– monissa aikaa osoittavissa ja partikkeleita (konjunktioita sekä muita
”pikkusanoja”) sisältävissä rakenteissa: 20 vuotta; alun perin, ennen kuin,
toissa kesänä, viime vuonna, muun muassa
""".strip()


def test_muu_muassa(asafi):
    asafi.omorfi_tokenise(MUU_MUASSA_TEXT)
    asafi.analyse_all()
    found_lextracts = 0
    for typ, payload in asafi.get_analyses_at(185):
        if typ != "lextract":
            continue
        found_lextracts += 1
        anchor = payload[0]
        assert anchor.bits[0] == AnalysisAnchorBit(form='muun', start=177, end=181)
        assert anchor.bits[1] == AnalysisAnchorBit(form='muassa', start=182, end=188)
    assert 1 <= found_lextracts <= 2


SPARTA_TEXT = """
Peloponnesolaissota käytiin pääasiassa Ateenan ja Spartan johtamien liittojen
välillä vuosina 431–404 eaa. Persialaissotien jälkeen Ateenan valta oli
kasvanut valtavasti, ja muut kaupunkivaltiot pelkäsivät, että Ateenasta tulisi
liian voimakas. Lopulta sota syttyi Spartan ja Ateenan välillä.
Peloponnesolaissota oli yksi antiikin Kreikan historian käännekohdista. Sota
veti mukaansa miltei kaikki kreikkalaiset kaupunkivaltiot, ja sen aikana kuoli
ennennäkemätön määrä ihmisiä. Sodan aikana kumpikin osapuoli teki myös
julmuuksia, joilta aikaisemmin oli s äästytty.
""".strip()


@pytest.mark.parametrize(
    "text,char_idx,child_seg,par_seg,hz",
    [
        (MUU_MUASSA_TEXT, 59, "-ta", "konjunktioita", [(11, 13), (11, 13)]),
        #(SPARTA_TEXT, 5, "Sparta", "Sparta", (0, 6)),
    ]
)
def test_sparta_align(asafi, text, char_idx, child_seg, par_seg, hz):
    asafi.omorfi_tokenise(text)
    asafi.analyse_all()
    found_tokens = 0
    for typ, payload in asafi.get_analyses_at(char_idx):
        if typ == "token":
            found_tokens += 1
            anchor, (root, terms, align) = payload
            assert len(terms) == 1
            term = terms[0]
            flattened = flatten_align_onto_anscestors(term, align)
            assert flattened[child_seg][par_seg] == hz
    assert found_tokens == 1
