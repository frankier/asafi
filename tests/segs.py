from asafi.engine import mk_omorfi_hang_engine as mk_engine
from asafi.rule.wiki import get_wiktionary_derivs
from asafi.db import get_session

from .utils import terminal_seg_strs, assert_zero_width


def test_voileipakakku_segmented():
    engine = mk_engine(get_session())
    root, terms, align = engine.analyse_tok("voileipäkakusta")
    all_segs_str = terminal_seg_strs(terms)
    assert ["voi", "leipä", "kakku", "sta"] in all_segs_str
    # This is a segmentation, but it should not be terminal
    assert ["voileipä", "kakku", "sta"] not in all_segs_str


def markkinoille_segmented():
    engine = mk_engine(get_session())
    root, terms, align = engine.analyse_tok("markkinoille")
    all_segs_str = terminal_seg_strs(terms)
    assert ["markkina", "lle"] in all_segs_str
    assert len(terms) == 1
    hz1 = terms[0].alignment_of(0, "markkina")
    assert hz1 is not None
    assert hz1.match_span == (0, 7)
    assert hz1.grey_span == (7, 9)
    hz2 = terms[0].alignment_of(1, "lle")
    assert hz2.match_span == (9, 12)
    assert_zero_width(hz2.grey_span)


def test_voimakkaammin_segmented():
    engine = mk_engine(get_session())
    root, terms, align = engine.analyse_tok("voimakkaammin")
    all_segs_str = terminal_seg_strs(terms)
    assert ["voida", "ma", "kas", "sti", "mpi"] in all_segs_str
    assert ["voida", "ma", "kas", "mpi", "t", "in"] in all_segs_str


def test_voimakkaammin_not_over_segmented():
    engine = mk_engine(get_session())
    root, terms, align = engine.analyse_tok("voimakkaammin")
    all_segs_str = terminal_seg_strs(terms)
    assert len(all_segs_str) == 2


def test_unique_segs_wiki_voileipakakku():
    wiki_derivs = get_wiktionary_derivs(get_session(), "voileipäkakku")
    found = False
    assert len(wiki_derivs["leipä"]) == 1
    for idx, poses, payload in wiki_derivs["leipä"][0].etys:
        if not payload:
            continue
        assert payload[0] == "inflection"
        assert len(payload[1]) == 2
        found = True
    assert found
