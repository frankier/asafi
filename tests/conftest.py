log_db = False
log = None
test_db = None


def pytest_addoption(parser):
    group = parser.getgroup("asafi")
    group.addoption(
        "--log-db",
        action="store_true",
        dest="log_db",
        default=False,
        help='Log database statements executed during test run',
    )
    group.addoption(
        "--override-test-db",
        dest="test_db",
        default=None,
        help='Run tests against another database',
    )


def pytest_configure(config):
    global log_db, log, test_db
    log_db = config.getoption("log_db")
    if log_db:
        log = open("dblog", "w")
    test_db = config.getoption("test_db")


outer_level = True


def receive_after_cursor_execute(**kw):
    global outer_level
    if not outer_level:
        return
    clauseelement = kw['clauseelement']

    print(clauseelement.compile(compile_kwargs={"literal_binds": True}), file=log)


def set_up_logger(session):
    from sqlalchemy import event
    print("SET UP LOGGER")
    event.listens_for(session.get_bind(), 'after_execute', named=True)(receive_after_cursor_execute)


def pytest_sessionstart(session):
    from asafi.db import install_post_init_hook, set_connstring
    if log_db:
        install_post_init_hook(set_up_logger)
    if test_db is not None:
        set_connstring(test_db)
    else:
        set_connstring("sqlite:///fixtures/enwiktionary-20190406.20200921.test.subset.db")
