import pytest

from asafi.align.group import MaxOverlapAlignmentGroup
from asafi.db import get_session
from asafi.engine import mk_omorfi_hang_engine as mk_engine

from .utils import assert_span_well_formed, assert_logical_spans, assert_match_lengths


@pytest.fixture(scope="module")
def engine():
    return mk_engine(get_session())


ALIGNMENTS = [
    ("voimakkaammin", ["voida", "ma", "kas", "sti", "mpi"], ((0, 3, 3), (3, 5, 2), (5, 9, 2), (9, 13, 0), (9, 13, 1))),
    ("konjunktioita", ['konjunktio', 't', 'ta'], ((0, 10, 10), (10, 11, 0), (11, 13, 2))),
    # TODO: There is allomorph information available here.
    #       For simple allomorph like t -> i (e/j?) we might like this information
    #       (it matched with a common morpheme)
    ("kotikonsoleille", ["koti", "konsoli", "t", "lle"], ((0, 4, 4), (4, 11, 6), (11, 12, 0), (12, 15, 3))),
    ("toimintaroolipeleiksi", ["toiminta", "rooli", "peli", "t", "ksi"], ((0, 8, 8), (8, 13, 5), (13, 17, 3), (17, 18, 0), (18, 21, 3))),
    ("suunnittelusta", ["suunnitella", "u", "sta"], ((0, 10, 7), (10, 11, 1), (11, 14, 3))),
]


@pytest.mark.parametrize(
    "surf,segs,expected_alignments",
    ALIGNMENTS
)
def test_alignments(engine, surf, segs, expected_alignments):
    root, terms, align = engine.analyse_tok(surf)
    chosen_term = None
    for term in terms:
        if list(term.segs.undasheds) == segs:
            chosen_term = term
    assert chosen_term is not None
    align = MaxOverlapAlignmentGroup(root)
    alignments = []
    for idx in range(len(segs)):
        alignments.append(align.alignment_of(chosen_term, idx, surf))
    assert_span_well_formed(alignments, surf, segs)
    assert_logical_spans(alignments, *(tpl[:2] for tpl in expected_alignments))
    assert_match_lengths(alignments, *(tpl[2] for tpl in expected_alignments))
