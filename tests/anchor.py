from pprint import pprint

from asafi.db import get_session
from asafi.paragraph_analysis import ParagraphAnalysis
from asafi.engine import mk_omorfi_hang_engine as mk_engine
from asafi.models import AnalysisAnchorBit


BANAANIKARPANEN = """
Vaikka banaanikärpänen on tavallinen vieras suomalaisissakin kodeissa, se ei pysty talvehtimaan Suomessa vaan kanta on riippuvainen Baltian maista saapuvista ja kauppojen hedelmien mukana tulevista yksilöistä. Ravinnokseen mahlakärpäslajit imevät käymistilassa olevia nesteitä, esimerkiksi hedelmien mehua ja mahlaa. Toukat elävät esimerkiksi mahlassa tai maahan pudonneissa marjoissa ja hedelmissä syöden nesteitä sekä hiivoja ja muita mikro-organismeja.
""".strip()


def test_banaanikarpanen():
    session = get_session()
    engine = mk_engine(session)
    para_anal = ParagraphAnalysis(engine, session)
    para_anal.omorfi_tokenise(BANAANIKARPANEN)
    para_anal.analyse_all()
    found_lextracts = 0
    for typ, payload in para_anal.get_analyses_at(80):
        if typ != "lextract":
            continue
        found_lextracts += 1
        anchor = payload[0]
        assert anchor.bits[0] == AnalysisAnchorBit(form='pysty', start=77, end=82)
        # TODO: bits[1].form WILDCARD
        assert anchor.bits[1].start == 83
        assert anchor.bits[1].end == 95
    assert found_lextracts == 1
