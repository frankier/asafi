import os
import click
from lextract.mweproc.core import WORDLIST_NAMES
from wikiparse.cmd.parse import mod_data_opt, fsts_dir_opt


def mkdb(metadata):
    from wikiparse.cmd.mk_db import mk_cmds
    cmds = mk_cmds(metadata)
    cmds.get_command(None, "create").callback()


@click.command()
@click.argument("indir", type=click.Path())
@click.argument("filterfile", type=click.File(mode="r"))
@click.argument("outdb")
@mod_data_opt
@fsts_dir_opt
def main(indir, filterfile, outdb):
    from lextract.keyed_db.builddb import add_keyed_words_cmd
    from lextract.mweproc.builddb import builddb
    from wikiparse.db.tables import metadata as wikiparse_metadata
    from wikiparse.cmd.parse import insert_dir
    os.environ["DATABASE_URL"] = "sqlite:///" + outdb
    mkdb(wikiparse_metadata)
    insert_dir.callback(indir, filterfile)
    filterfile.seek(0)
    builddb.callback(True, True, WORDLIST_NAMES, filterfile)
    add_keyed_words_cmd.callback(True, True, False)


if __name__ == "__main__":
    main()
