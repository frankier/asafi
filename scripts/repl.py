import os
import sys
import click
from pprint import pprint

from asafi.engine import mk_omorfi_hang_engine as mk_engine
from asafi.paragraph_analysis import ParagraphAnalysis
from wikiparse.utils.db import get_session


def disp_graph(root, fn, edge_label=False):
    if edge_label:
        kwargs = dict(do_edge_label=True, do_tooltip=False)
    else:
        kwargs = dict(do_edge_label=False, do_tooltip=True)
    root.draw(f"{fn}.dot", **kwargs)
    os.system(f"dot {fn}.dot -Tsvg -o{fn}.svg")
    os.system(f"firefox {fn}.svg")


@click.group()
@click.option("--graph/--no-graph")
@click.option("--edge-label/--tooltip")
@click.pass_context
def repl(ctx, graph, edge_label):
    from lextract.keyed_db.tables import extend_mweproc
    extend_mweproc()
    ctx.ensure_object(dict)
    ctx.obj['graph'] = graph
    ctx.obj['edge_label'] = edge_label


@repl.command("one")
@click.pass_context
def one(ctx):
    engine = mk_engine(get_session())
    word_form = input("Enter a single word >> ").strip()
    root, terms, align = engine.analyse_tok(word_form)
    print()
    print("###")
    print()
    for term in terms:
        print(" ".join(term.segs.undasheds))
    for term in terms:
        pprint(term)
    if ctx.obj['graph']:
        disp_graph(root, "one", ctx.obj['edge_label'])


@repl.command("all")
@click.pass_context
def all(ctx):
    session = get_session()
    engine = mk_engine(session)
    paragraph = sys.stdin.read()
    para_anal = ParagraphAnalysis(engine, session)
    para_anal.omorfi_tokenise(paragraph)
    for analysis in para_anal.get_all_analyses():
        pprint(analysis)


@repl.command("para")
@click.pass_context
def para(ctx):
    lines = []
    blanks = 0
    while 1:
        inp = input()
        lines.append(inp)
        if not inp.strip():
            blanks += 1
            if blanks >= 2:
                break
    paragraph = "\n".join(lines).strip()
    print(paragraph)
    session = get_session()
    engine = mk_engine(session)
    para_anal = ParagraphAnalysis(engine, session)
    para_anal.omorfi_tokenise(paragraph)
    para_anal.analyse_all()
    while 1:
        position = int(input("Position >> "))
        for idx, anal in enumerate(para_anal.get_analyses_at(position)):
            pprint(anal)
            if ctx.obj['graph']:
                disp_graph(anal[1][0], f"all{idx}", ctx.obj['edge_label'])


if __name__ == "__main__":
    repl()
